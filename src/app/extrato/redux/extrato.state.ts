import { ILogDerivativo } from './../../shared/interfaces/LogDerivativo';
import { IExtratoDerivativo } from '@shared/interfaces/ExtratoDerivativo';

export class ExtratoState {
  date: Date = new Date();
  logDerivativos: ILogDerivativo[] = [];
  extratos: IExtratoDerivativo[] = [];
  search = '';
  update = false;
}
