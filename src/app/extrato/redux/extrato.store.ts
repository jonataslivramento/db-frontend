import { ILogDerivativo } from './../../shared/interfaces/LogDerivativo';
import { Injectable } from '@angular/core';
import { Store } from '@shared/helpers/store';
import { IExtratoDerivativo } from '@shared/interfaces/ExtratoDerivativo';

import { ExtratoState } from './extrato.state';

@Injectable({
  providedIn: 'root',
})
export class ExtratoStore extends Store<ExtratoState> {
  constructor() {
    super(new ExtratoState());
  }

  changeSearch(search: string): void {
    this.setState({
      ...this.state,
      search,
    });
  }

  changeExtratos(extratos: IExtratoDerivativo[]): void {
    this.setState({
      ...this.state,
      extratos,
    });
  }

  updateExtratos(update: boolean): void {
    this.setState({
      ...this.state,
      update,
    });
  }

  changeDate(date: Date): void {
    this.setState({
      ...this.state,
      date
    });
  }

  changeLogs(logDerivativos: ILogDerivativo[]): void {
    this.setState({
      ...this.state,
      logDerivativos,
    });
  }

  updateLogs(update: boolean): void {
    this.setState({
      ...this.state,
      update,
    });
  }
}
