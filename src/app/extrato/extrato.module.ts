import { ExtratoResponseDialogComponent } from './geracao-derivativo/extrato-response-dialog/extrato-response-dialog.component';
import { ExtratoDialogListComponent } from './geracao-derivativo/extrato-dialog/extrato-dialog-list/extrato-dialog-list.component';
import { SharedModule } from '@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtratoRoutingModule } from './extrato-routing.module';
import { ExtratoComponent } from './extrato.component';
import { ExtratoSearchComponent } from './geracao-derivativo/extrato-search/extrato-search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExtratoListComponent } from './geracao-derivativo/extrato-list/extrato-list.component';
import { ExtratoReportComponent } from './geracao-derivativo/extrato-report/extrato-report.component';
import { ExtratoDialogComponent } from './geracao-derivativo/extrato-dialog/extrato-dialog.component';
import { ExtratoDialogAgendamentoComponent } from './geracao-derivativo/extrato-dialog/extrato-dialog-agendamento/extrato-dialog-agendamento.component';
import { GeracaoDerivativoComponent } from './geracao-derivativo/geracao-derivativo.component';
import { LogDerivativoComponent } from './log-derivativo/log-derivativo.component';
import { LogSearchComponent } from './log-derivativo/log-search/log-search.component';
import { LogListComponent } from './log-derivativo/log-list/log-list.component';

@NgModule({
  declarations: [
    ExtratoComponent,
    ExtratoSearchComponent,
    ExtratoListComponent,
    ExtratoReportComponent,
    ExtratoDialogComponent,
    ExtratoResponseDialogComponent,
    ExtratoDialogListComponent,
    ExtratoDialogAgendamentoComponent,
    GeracaoDerivativoComponent,
    LogDerivativoComponent,
    LogSearchComponent,
    LogListComponent,
  ],
  imports: [CommonModule, SharedModule, FormsModule, ReactiveFormsModule, ExtratoRoutingModule],
})
export class ExtratoModule {}
