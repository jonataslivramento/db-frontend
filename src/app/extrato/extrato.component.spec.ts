import { ExtratoSearchComponent } from './geracao-derivativo/extrato-search/extrato-search.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtratoComponent } from './extrato.component';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ExtratoComponent', () => {
  let component: ExtratoComponent;
  let extratoSearchComponent: ExtratoSearchComponent;
  let fixture: ComponentFixture<ExtratoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExtratoComponent, ExtratoSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtratoComponent);
    component = fixture.componentInstance;
    extratoSearchComponent = fixture.debugElement
    .query(By.directive(ExtratoSearchComponent))
    .componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
