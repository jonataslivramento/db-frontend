import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { DestroyService } from '@shared/services/destroy/destroy.service';
import { takeUntil } from 'rxjs/operators';
import { ExtratoStore } from '../../redux/extrato.store';

@Component({
  selector: 'app-log-search',
  templateUrl: './log-search.component.html',
  styleUrls: ['./log-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DestroyService],
})
export class LogSearchComponent implements OnInit, OnDestroy {
  date = new Date();
  dataAtual = new Date();

  constructor(private store: ExtratoStore, private destroy$: DestroyService) {}

  ngOnInit(): void {
    this.store.state$.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      this.date = data.date;
    });
  }

  ngOnDestroy(): void {
    this.store.changeSearch('');
  }

  handleDate(): void {
    this.store.changeDate(this.date);
  }
}
