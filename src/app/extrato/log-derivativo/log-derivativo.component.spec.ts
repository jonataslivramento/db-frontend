import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogDerivativoComponent } from './log-derivativo.component';

describe('LogDerivativoComponent', () => {
  let component: LogDerivativoComponent;
  let fixture: ComponentFixture<LogDerivativoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogDerivativoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogDerivativoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
