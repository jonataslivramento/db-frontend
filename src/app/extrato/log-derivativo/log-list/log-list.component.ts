import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { itensPorPagina } from '@shared/constants/index';
import { ILogDerivativo } from '@shared/interfaces/LogDerivativo';
import { DestroyService } from '@shared/services/destroy/destroy.service';
import { ExtratoService } from '@shared/services/extrato/extrato.service';
import { Observable } from 'rxjs/internal/Observable';
import { takeUntil } from 'rxjs/operators';
import { ExtratoDialogComponent } from '../../geracao-derivativo/extrato-dialog/extrato-dialog.component';
import { ExtratoStore } from '../../redux/extrato.store';
import { colunasLogDerivativo } from './../../../shared/constants/index';
import { ExtratoState } from './../../redux/extrato.state';

@Component({
  selector: 'app-log-list',
  templateUrl: './log-list.component.html',
  styleUrls: ['./log-list.component.scss'],
  providers: [DestroyService],
})
export class LogListComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  search: string;
  totalElements: number;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions = itensPorPagina;
  state: ExtratoState;
  errorMessage = '';
  errorDate = new Date();

  extratos: ILogDerivativo[] = [];

  displayedColumns: string[] = colunasLogDerivativo;
  dataSource: MatTableDataSource<ILogDerivativo>;
  selection = new SelectionModel<ILogDerivativo>(true, []);

  constructor(
    private extratoService: ExtratoService,
    private destroy$: DestroyService,
    private store: ExtratoStore,
    public dialog: MatDialog
  ) {
    this.dataSource = new MatTableDataSource<ILogDerivativo>(this.extratos);
  }

  ngOnInit(): void {
    this.store.state$
      .pipe(takeUntil(this.destroy$))
      .subscribe((state: ExtratoState) => {
        this.state = state;
        if (state.date) {
          this.pageIndex = 0;
          this.paginator.firstPage();
          this.getLogDerivativos();
        }
      });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  getLogDerivativos(): void {
    const parameters = {
      date: `${this.state.date.getFullYear()}-${
        this.state.date.getMonth() + 1
      }-${this.state.date.getDate()}`,
      page: this.pageIndex.toLocaleString().replace(/,/g, ''),
      size: this.pageSize.toLocaleString(),
    };
    this.extratoService
      .getLogDerivativos(parameters)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (response: any) => {
          this.resetErrors();
          this.extratos = response.content;
          this.totalElements = response.totalElements;
          this.updateDataSource();
        },
        (e) => {
          const { error } = e;
          this.extratos = [];
          this.errorMessage = error.message;
          this.errorDate = this.state.date;
        }
      );
  }

  resetErrors(): void {
    this.errorDate = new Date();
    this.errorMessage = '';
  }

  updateDataSource(): void {
    this.dataSource.data = this.extratos;
    this.selection = new SelectionModel<ILogDerivativo>(true, []);
  }

  handleChangePage(pageEvent: PageEvent): void {
    this.pageIndex = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;

    this.getLogDerivativos();
  }
}
