import { LogDerivativoComponent } from './log-derivativo/log-derivativo.component';
import { GeracaoDerivativoComponent } from './geracao-derivativo/geracao-derivativo.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'geracao',
    data: { breadcrumb: 'Geração Extrato' },
    component: GeracaoDerivativoComponent,
  },
  {
    path: 'log',
    data: { breadcrumb: 'Log de Processamento' },
    component: LogDerivativoComponent,
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExtratoRoutingModule { }
