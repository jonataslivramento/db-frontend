import { OnDestroy } from '@angular/core';
import { map, takeUntil } from 'rxjs/operators';
import { DestroyService } from '@shared/services/destroy/destroy.service';
import { ExtratoStore } from '../../redux/extrato.store';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'app-extrato-search',
  templateUrl: './extrato-search.component.html',
  styleUrls: ['./extrato-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DestroyService],
})
export class ExtratoSearchComponent implements OnInit, OnDestroy {
  search = '';

  constructor(
    private store: ExtratoStore,
    private destroy$: DestroyService
  ) { }

  ngOnInit(): void {
    this.store.state$.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      this.search = data.search;
    });
  }

  ngOnDestroy(): void {
    this.store.changeSearch('');
  }

  handleSearch(): void {
    this.store.changeSearch(this.search);
  }
}
