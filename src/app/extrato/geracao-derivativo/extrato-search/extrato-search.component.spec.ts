import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtratoSearchComponent } from './extrato-search.component';

describe('ExtratoSearchComponent', () => {
  let component: ExtratoSearchComponent;
  let fixture: ComponentFixture<ExtratoSearchComponent>;
  let nativeElement: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExtratoSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtratoSearchComponent);
    component = fixture.componentInstance;
    nativeElement = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
