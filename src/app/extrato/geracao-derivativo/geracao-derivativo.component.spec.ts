import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeracaoDerivativoComponent } from './geracao-derivativo.component';

describe('GeracaoDerivativoComponent', () => {
  let component: GeracaoDerivativoComponent;
  let fixture: ComponentFixture<GeracaoDerivativoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeracaoDerivativoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeracaoDerivativoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
