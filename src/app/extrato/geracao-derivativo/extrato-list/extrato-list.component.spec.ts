import { IExtratoDerivativo } from '@shared/interfaces/ExtratoDerivativo';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';

import { ExtratoListComponent } from './extrato-list.component';

const extratos: IExtratoDerivativo[] = [
  {
    isInsertedOnExtract: false,
    cdCliente: 1,
    nomeCliente: 'Claudia',
    cpfCnpjCliente: '32477387000168',
    vrAgendamento: '',
    vrPeriodo: 'MENSAL',
    vrHorario: 'MANHA',
    vrDiaSemana: 'SEGUNDA',
    dtRef: '28',
    vrTipoRelatorio: 'PDF',
    vrEmailFlag: 'S',
    cdSitPessoa: 'A',
  },
  {
    isInsertedOnExtract: false,
    cdCliente: 2,
    nomeCliente: 'Marcio',
    cpfCnpjCliente: '32477387000168',
    vrAgendamento: '',
    vrPeriodo: 'MENSAL',
    vrHorario: 'MANHA',
    vrDiaSemana: 'SEGUNDA',
    dtRef: '28',
    vrTipoRelatorio: 'PDF',
    vrEmailFlag: 'S',
    cdSitPessoa: 'A',
  },
];

describe('ExtratoListComponent', () => {
  let component: ExtratoListComponent;
  let fixture: ComponentFixture<ExtratoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatDialogModule, HttpClientTestingModule],
      declarations: [ExtratoListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtratoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(
    `should toggle all lines`,
    waitForAsync(() => {
      component.ngOnInit();
      component.dataSource = new MatTableDataSource<IExtratoDerivativo>(
        extratos
      );
      component.toggleAll();
      fixture.whenStable().then(() => {
        expect(component.selection.selected.length).toEqual(2);
        component.toggleAll();
        fixture.whenStable().then(() => {
          expect(component.selection.selected.length).toEqual(0);
        });
      });
    })
  );

  it(
    `should add checkbox label to select a row`,
    waitForAsync(() => {
      component.ngOnInit();
      const checkboxLabel = component.checkboxLabel(extratos[0]);
      fixture.whenStable().then(() => {
        expect(checkboxLabel).toBe(`select row ${extratos[0].nomeCliente}`);
      });
    })
  );

  it(
    `should add checkbox label to deselect a row`,
    waitForAsync(() => {
      component.selection.select(extratos[0]);
      const checkboxLabel = component.checkboxLabel(extratos[0]);
      fixture.whenStable().then(() => {
        expect(checkboxLabel).toBe(`deselect row ${extratos[0].nomeCliente}`);
      });
    })
  );

  it(
    `should add checkbox label to deselect all rows`,
    waitForAsync(() => {
      const checkboxLabel = component.checkboxLabel();
      fixture.whenStable().then(() => {
        expect(checkboxLabel).toBe(`select all`);
      });
    })
  );

  it(
    `should add checkbox label to select all rows`,
    waitForAsync(() => {
      component.ngOnInit();
      component.dataSource = new MatTableDataSource<IExtratoDerivativo>(
        extratos
      );
      component.toggleAll();
      const checkboxLabel = component.checkboxLabel();
      fixture.whenStable().then(() => {
        expect(checkboxLabel).toBe(`select all`);
      });
    })
  );
});
