import { GridDerivativoManutencaoParameters } from '../../../shared/interfaces/GridDerivativoManutencaoParameters';
import { itensPorPagina } from '../../../shared/constants/index';
import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, ComponentFactoryResolver, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { IExtratoDerivativo } from '@shared/interfaces/ExtratoDerivativo';
import { map, takeUntil } from 'rxjs/operators';
import { DestroyService } from '../../../shared/services/destroy/destroy.service';
import { ExtratoService } from '../../../shared/services/extrato/extrato.service';
import { ExtratoDialogComponent } from '../extrato-dialog/extrato-dialog.component';
import { ExtratoStore } from '../../redux/extrato.store';

@Component({
  selector: 'app-extrato-list',
  templateUrl: './extrato-list.component.html',
  styleUrls: ['./extrato-list.component.scss'],
  providers: [DestroyService],
})
export class ExtratoListComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;

  search: string;
  totalElements: number;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions = itensPorPagina;

  extratos: IExtratoDerivativo[] = [];

  displayedColumns: string[] = [
    'select',
    'cdCliente',
    'nomeCliente',
    'cpfCnpjCliente',
    'vrPeriodo',
  ];
  dataSource: MatTableDataSource<IExtratoDerivativo>;
  selection = new SelectionModel<IExtratoDerivativo>(true, []);

  constructor(
    private extratoService: ExtratoService,
    private destroy$: DestroyService,
    private store: ExtratoStore,
    public dialog: MatDialog
  ) {
    this.dataSource = new MatTableDataSource<IExtratoDerivativo>(this.extratos);
  }

  ngOnInit(): void {
    this.getExtratoDerivativo();
    this.store.state$.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      this.dataSource.filter = data.search.trim().toLowerCase();
      this.search = data.search;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (data, filter: string): boolean => {
      return data.nomeCliente.toLowerCase().includes(filter);
    };
  }

  getExtratoDerivativo(): void {
    const parameters: GridDerivativoManutencaoParameters = {
      inserted: 'S',
      page: this.pageIndex.toLocaleString().replace(/,/g, ''),
      size: this.pageSize.toLocaleString(),
    };
    this.extratoService
      .getExtratoDerivativo(parameters)
      .pipe(takeUntil(this.destroy$))
      .subscribe((response: any) => {
        this.extratos = response.content;
        this.totalElements = response.totalElements;
        this.updateDataSource();
      });
  }

  updateDataSource(): void {
    this.dataSource.data = this.extratos;
    this.selection = new SelectionModel<IExtratoDerivativo>(true, []);
  }

  handleChangePage(pageEvent: PageEvent): void {
    this.pageIndex = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;

    this.getExtratoDerivativo();
  }

  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  toggleAll(): void {
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.dataSource.data.forEach((row) => this.selection.select(row));
    }
    this.store.changeExtratos(this.selection.selected);
  }

  toggleRow(extrato: IExtratoDerivativo): void {
    this.selection.toggle(extrato);
    this.store.changeExtratos(this.selection.selected);
  }

  checkboxLabel(row?: IExtratoDerivativo): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.nomeCliente
    }`;
  }

  openMaintenanceDialog(): void {
    const dialogRef = this.dialog.open(ExtratoDialogComponent, {
      data: this.selection.selected,
      width: '95vw',
      maxWidth: '95vw',
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.pageIndex = 0;
      this.getExtratoDerivativo();
    });
  }
}
