import { ExtratoDialogListComponent } from './extrato-dialog-list/extrato-dialog-list.component';
import { ExtratoResponseDialogComponent } from './../extrato-response-dialog/extrato-response-dialog.component';
import { ExtratoStore } from '../../redux/extrato.store';
import { ExtratoService } from '@shared/services/extrato/extrato.service';
import { IExtratoDerivativo } from '../../../shared/interfaces/ExtratoDerivativo';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DestroyService } from '@shared/services/destroy/destroy.service';
import { Observable } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { DialogService } from '../../../shared/services/dialog/dialog.service';
import { ExtratoDialogAgendamentoComponent } from './extrato-dialog-agendamento/extrato-dialog-agendamento.component';

@Component({
  templateUrl: './extrato-dialog.component.html',
  styleUrls: ['./extrato-dialog.component.scss'],
  providers: [DestroyService],
})
export class ExtratoDialogComponent implements OnInit {
  @ViewChild('extratoDialogList') extratoDialogList: ExtratoDialogListComponent;
  extratos: IExtratoDerivativo[] = [];

  constructor(
    private dialogService: DialogService,
    private extratoService: ExtratoService,
    private store: ExtratoStore,
    private dialog: MatDialog,
    private destroy$: DestroyService,
    public dialogRef: MatDialogRef<ExtratoDialogComponent>
  ) {}

  ngOnInit(): void {}

  adicionarCliente(): void {
    const dialog = this.dialogService.openConfirmationDialog({
      titulo: 'Adicionar da lista de extratos:',
      acao: 'Adicionar',
      dados: this.extratos,
      format: {
        key: 'cdCliente',
        value: 'nomeCliente',
      },
    });

    dialog.pipe(takeUntil(this.destroy$)).subscribe((response) => {
      if (response) {
        this.extratoService
          .updateExtratos(this.extratos)
          .pipe(take(1))
          .subscribe((serviceResponse) => {
            this.updateList();
            this.dialog.open(ExtratoResponseDialogComponent, {
              data: {
                titulo: 'Clientes adicionados ao extrato:',
                response: serviceResponse,
              },
            });
          });
      }
    });
  }

  removerCliente(): void {
    const dialog: Observable<any> = this.dialogService.openConfirmationDialog({
      titulo: 'Remover da lista de extratos:',
      acao: 'Remover',
      dados: this.extratos,
      format: {
        key: 'cdCliente',
        value: 'nomeCliente',
      },
    });

    dialog.pipe(takeUntil(this.destroy$)).subscribe((response) => {
      if (response) {
        this.extratoService
          .removerExtratoDerivativo(this.extratos)
          .pipe(take(1))
          .subscribe((serviceResponse) => {
            this.updateList();
            if (!serviceResponse) {
              return;
            }

            this.dialogService.openSuccessDialog({
              titulo: 'Clientes Removidos:',
              dados: serviceResponse,
              format: {
                key: 'cdCliente',
                value: 'nomeCliente',
              },
            });
          });
      }
    });
  }

  adicionarAgendamento(): void {
    const dialogRef = this.dialog.open(ExtratoDialogAgendamentoComponent, {
      disableClose: true,
    });

    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe((response) => {
        if (response) {
          this.extratos = this.extratos.map(
            (extrato) => (extrato = { ...extrato, ...response })
          );
          this.extratoService
            .updateExtratos(this.extratos)
            .pipe(take(1))
            .subscribe((serviceResponse) => {
              this.updateList();
              this.dialog.open(ExtratoResponseDialogComponent, {
                data: {
                  titulo: 'Agendamento adicionado para:',
                  response: serviceResponse,
                },
              });
            });
        }
      });
  }

  removerAgendamento(): void {
    const dialog = this.dialogService.openConfirmationDialog({
      titulo: 'Remover o agendamento para:',
      acao: 'Remover',
      dados: this.extratos,
      format: {
        key: 'cdCliente',
        value: 'nomeCliente',
      },
    });

    dialog.pipe(takeUntil(this.destroy$)).subscribe((response) => {
      if (response) {
        this.extratoService
          .removerAgendamentos(this.extratos)
          .pipe(take(1))
          .subscribe((serviceResponse) => {
            this.updateList();
            if (!serviceResponse) {
              return;
            }

            this.dialogService.openSuccessDialog({
              titulo: 'Agendamento Removido:',
              dados: serviceResponse,
              format: {
                key: 'cdCliente',
                value: 'nomeCliente',
              },
            });
          });
      }
    });
  }

  handleClientSelect(extratosDerivativos: IExtratoDerivativo[]): void {
    this.extratos = extratosDerivativos.map((extrato) => extrato);
  }

  close(): void {
    this.dialogRef.close();
  }

  updateList(): void {
    this.store.updateExtratos(true);
    this.extratos = [];
  }

  get disableAdicionarCliente(): boolean {
    if (
      this.extratos.length > 0 &&
      this.extratos.every((extrato) => !extrato.isInsertedOnExtract)
    ) {
      return false;
    } else {
      return true;
    }
  }

  get disableRemoverCliente(): boolean {
    if (
      this.extratos.length > 0 &&
      this.extratos.every((extrato) => extrato.isInsertedOnExtract)
    ) {
      return false;
    } else {
      return true;
    }
  }

  get disableAdicionarAgendamento(): boolean {
    if (
      this.extratos.length > 0 &&
      this.extratos.every((extrato) => extrato.isInsertedOnExtract)
    ) {
      return false;
    } else {
      return true;
    }
  }

  get disableRemoverAgendamento(): boolean {
    if (
      this.extratos.length > 0 &&
      this.extratos.every((extrato) => extrato.isInsertedOnExtract) &&
      this.extratos.every((extrato) => extrato.vrPeriodo) &&
      this.extratos.every((extrato) => extrato.vrPeriodo !== 'NENHUM')
    ) {
      return false;
    } else {
      return true;
    }
  }
}
