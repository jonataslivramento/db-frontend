import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { ComponentFixture, flush, TestBed, tick } from '@angular/core/testing';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { IExtratoDerivativo } from '@shared/interfaces/ExtratoDerivativo';
import { of } from 'rxjs';
import { IConfirmationDialog } from '../../../shared/interfaces/ConfirmationDialog';
import { DialogService } from '../../../shared/services/dialog/dialog.service';
import { ExtratoDialogComponent } from './extrato-dialog.component';

const extratosDerivativos: IExtratoDerivativo[] = [
  {
    isInsertedOnExtract: false,
    cdCliente: 1,
    nomeCliente: 'Claudia',
    cpfCnpjCliente: '32477387000168',
    vrAgendamento: '',
    vrPeriodo: 'MENSAL',
    vrHorario: 'MANHA',
    vrDiaSemana: 'SEGUNDA',
    dtRef: '28',
    vrTipoRelatorio: 'PDF',
    vrEmailFlag: 'S',
    cdSitPessoa: 'A',
  },
  {
    isInsertedOnExtract: false,
    cdCliente: 2,
    nomeCliente: 'Marcio',
    cpfCnpjCliente: '32477387000168',
    vrAgendamento: '',
    vrPeriodo: 'MENSAL',
    vrHorario: 'MANHA',
    vrDiaSemana: 'SEGUNDA',
    dtRef: '28',
    vrTipoRelatorio: 'PDF',
    vrEmailFlag: 'S',
    cdSitPessoa: 'A',
  },
];

class DialogServiceMock {
  openConfirmationDialog(
    confirmationOptions: IConfirmationDialog,
    result: boolean = true
  ) {
    return {
      afterClosed: () => of(result),
    };
  }

  openSuccessDialog() {
    return;
  }
}

describe('ExtratoDialogComponent', () => {
  let component: ExtratoDialogComponent;
  let fixture: ComponentFixture<ExtratoDialogComponent>;
  let debugElement: DebugElement;
  let dialogService: DialogService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatDialogModule, HttpClientTestingModule],
      declarations: [ExtratoDialogComponent],

      providers: [
        {
          provide: DialogService,
          useClass: DialogServiceMock,
        },
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtratoDialogComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    dialogService = TestBed.inject(DialogService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call adicionarCliente and open Confirmation Dialog', () => {
    spyOn(dialogService, 'openConfirmationDialog').and.returnValue(of(true));
    component.adicionarCliente();
    fixture.detectChanges();

    expect(dialogService.openConfirmationDialog).toHaveBeenCalled();
  });

  it('should call removerCliente and open Confirmation Dialog', () => {
    spyOn(dialogService, 'openConfirmationDialog').and.returnValue(of(true));
    component.removerCliente();
    fixture.detectChanges();

    expect(dialogService.openConfirmationDialog).toHaveBeenCalled();
  });

  it('should call removerAgendamento and open Confirmation Dialog', () => {
    spyOn(dialogService, 'openConfirmationDialog').and.returnValue(of(true));
    component.removerAgendamento();
    fixture.detectChanges();

    expect(dialogService.openConfirmationDialog).toHaveBeenCalled();
  });

  it('should update clientes when new clientes is provided', () => {
    component.extratos = [];
    component.handleClientSelect(extratosDerivativos);
    fixture.detectChanges();
    expect(component.extratos.length).toBeGreaterThanOrEqual(
      extratosDerivativos.length
    );
  });

  it('should enable to add extratos', () => {
    component.extratos = [];
    component.handleClientSelect(extratosDerivativos);
    fixture.detectChanges();
    expect(component.disableAdicionarCliente).toBeFalse();
  });

  it('should enable to remove extratos', () => {
    component.extratos = [];
    component.handleClientSelect(extratosDerivativos);
    fixture.detectChanges();
    expect(component.disableRemoverCliente).toBeTrue();
  });

  it('should enable to add new "agendamentos"', () => {
    component.extratos = [];
    component.handleClientSelect(extratosDerivativos);
    fixture.detectChanges();
    expect(component.disableAdicionarAgendamento).toBeTrue();
  });

  it('should enable to add remove "agendamentos"', () => {
    component.extratos = [];
    component.handleClientSelect(extratosDerivativos);
    fixture.detectChanges();
    expect(component.disableRemoverAgendamento).toBeTrue();
  });
});
