import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  DIA_SEMANA_AGENDAMENTO_ENUM,
  FREQUENCIAS_ENUM,
  TIPO_RELATORIO_ENUM,
} from '../../../../shared/constants/agendamento';
import { IClienteManutencao } from '../../../../shared/interfaces/ClienteManutencao';
import { IExtratoDerivativo } from '../../../../shared/interfaces/ExtratoDerivativo';
import { HORARIO_AGENDAMENTO_HORARIO_ENUM } from './../../../../shared/constants/agendamento';

@Component({
  templateUrl: './extrato-dialog-agendamento.component.html',
  styleUrls: ['./extrato-dialog-agendamento.component.scss'],
})
export class ExtratoDialogAgendamentoComponent implements OnInit {
  agendamentoForm: FormGroup;

  frequencias = [
    FREQUENCIAS_ENUM.DIARIO,
    FREQUENCIAS_ENUM.SEMANAL,
    FREQUENCIAS_ENUM.MENSAL,
  ];
  horariosAgendamento = [
    HORARIO_AGENDAMENTO_HORARIO_ENUM.MANHA,
    HORARIO_AGENDAMENTO_HORARIO_ENUM.TARDE,
    HORARIO_AGENDAMENTO_HORARIO_ENUM.NOITE,
  ];
  diasSemanaAgendamento = [
    DIA_SEMANA_AGENDAMENTO_ENUM.SEGUNDA,
    DIA_SEMANA_AGENDAMENTO_ENUM.TERÇA,
    DIA_SEMANA_AGENDAMENTO_ENUM.QUARTA,
    DIA_SEMANA_AGENDAMENTO_ENUM.QUINTA,
    DIA_SEMANA_AGENDAMENTO_ENUM.SEXTA,
  ];

  diasDisponiveisAgendamento = Array.from({ length: 31 }, (_, i) => i + 1);

  clienteManutencao: IClienteManutencao;
  pdfCheckbox = false;
  excelCheckbox = false;

  constructor(
    public dialogRef: MatDialogRef<ExtratoDialogAgendamentoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IExtratoDerivativo[],
    private formBuilder: FormBuilder
  ) {
    this.agendamentoForm = this.formBuilder.group({
      vrPeriodo: [null, Validators.required],
      vrHorario: [null, Validators.required],
      vrDiaSemana: [{ value: null, disabled: true }],
      dtRef: [{ value: null, disabled: true }],
      vrTipoRelatorio: [null, Validators.required],
    });
    this.setAgendamentoFormValidators();
  }

  ngOnInit(): void {}

  setAgendamentoFormValidators(): void {
    const diaSemanaControl = this.agendamentoForm.get('vrDiaSemana');
    const agendamentoControl = this.agendamentoForm.get('dtRef');

    this.agendamentoForm.get('vrPeriodo')?.valueChanges.subscribe((periodo) => {
      this.clearWhenPeriodoChanges();
      if (periodo === FREQUENCIAS_ENUM.SEMANAL) {
        diaSemanaControl?.setValidators([Validators.required]);
        diaSemanaControl?.enable();
        agendamentoControl?.setValidators(null);
        agendamentoControl?.disable();
      }

      if (periodo === FREQUENCIAS_ENUM.MENSAL) {
        diaSemanaControl?.setValidators(null);
        diaSemanaControl?.disable();
        agendamentoControl?.setValidators([Validators.required]);
        agendamentoControl?.enable();
      }

      if (periodo === FREQUENCIAS_ENUM.DIARIO) {
        diaSemanaControl?.setValidators(null);
        diaSemanaControl?.disable();
        agendamentoControl?.setValidators(null);
        agendamentoControl?.disable();
      }

      diaSemanaControl?.updateValueAndValidity();
      agendamentoControl?.updateValueAndValidity();
    });

  }

  clearWhenPeriodoChanges(): void {
    this.agendamentoForm.patchValue({
      vrDiaSemana: null,
      dtRef: null,
    });
  }

  get diaSemanaHabilitado(): boolean {
    return (
      this.agendamentoForm.get('vrPeriodo')?.value !== FREQUENCIAS_ENUM.SEMANAL
    );
  }

  get diaMesHabilitado(): boolean {
    return (
      this.agendamentoForm.get('vrPeriodo')?.value !== FREQUENCIAS_ENUM.MENSAL
    );
  }

  salvarAgendamento(): void {
    this.assignTipoRelatorio();
    if (this.agendamentoForm.valid) {
      this.dialogRef.close(this.agendamentoForm.value);
    }
  }

  handleCheckboxChange(event: MatCheckboxChange, checkbox: string): void {
    if (checkbox === 'pdfCheckbox') {
      this.pdfCheckbox = event.checked;
    } else {
      this.excelCheckbox = event.checked;
    }
    this.assignTipoRelatorio();
  }

  assignTipoRelatorio(): void {
    if (this.pdfCheckbox && this.excelCheckbox) {
      this.agendamentoForm.patchValue({
        vrTipoRelatorio: TIPO_RELATORIO_ENUM.AMBOS,
      });
    } else if (this.pdfCheckbox) {
      this.agendamentoForm.patchValue({
        vrTipoRelatorio: TIPO_RELATORIO_ENUM.PDF,
      });
    } else if (this.excelCheckbox) {
      this.agendamentoForm.patchValue({
        vrTipoRelatorio: TIPO_RELATORIO_ENUM.EXCEL,
      });
    } else {
      this.agendamentoForm.patchValue({
        vrTipoRelatorio: null,
      });
    }
  }
}
