import { HarnessLoader } from '@angular/cdk/testing';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HorarioAgendamentoPipe } from './../../../../shared/pipes/horario-agendamento/horario-agendamento.pipe';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { TIPO_RELATORIO_ENUM } from './../../../../shared/constants/agendamento';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';

import { ExtratoDialogAgendamentoComponent } from './extrato-dialog-agendamento.component';
import { MatSelectModule } from '@angular/material/select';
import { By } from '@angular/platform-browser';
import { MatCheckboxModule } from '@angular/material/checkbox';

describe('ExtratoDialogAgendamentoComponent', () => {
  let component: ExtratoDialogAgendamentoComponent;
  let loader: HarnessLoader;
  let fixture: ComponentFixture<ExtratoDialogAgendamentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatCheckboxModule,
      ],
      declarations: [ExtratoDialogAgendamentoComponent, HorarioAgendamentoPipe],
      providers: [
        FormBuilder,
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtratoDialogAgendamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    loader = TestbedHarnessEnvironment.documentRootLoader(fixture);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should assign tipoRelatorio to EXCEL when only Excel checkbox is checked`, () => {
    component.excelCheckbox = true;
    component.assignTipoRelatorio();
    fixture.whenStable().then(() => {
      expect(component.agendamentoForm.get('vrTipoRelatorio')?.value).toBe(
        TIPO_RELATORIO_ENUM.EXCEL
      );
    });
  });

  it(`should assign tipoRelatorio to PDF  when only PDF checkbox is checked`, () => {
    component.pdfCheckbox = true;
    component.assignTipoRelatorio();
    fixture.whenStable().then(() => {
      expect(component.agendamentoForm.get('vrTipoRelatorio')?.value).toBe(
        TIPO_RELATORIO_ENUM.PDF
      );
    });
  });

  it(`should assign tipoRelatorio to AMBOS when  Excel checkbox and Pdf Checkbox are checked`, () => {
    component.excelCheckbox = true;
    component.pdfCheckbox = true;
    component.assignTipoRelatorio();
    fixture.whenStable().then(() => {
      expect(component.agendamentoForm.get('vrTipoRelatorio')?.value).toBe(
        TIPO_RELATORIO_ENUM.AMBOS
      );
    });
  });

  it(`should assign tipoRelatorio to null when Excel checkbox and Pdf Checkbox aren't checked`, () => {
    component.assignTipoRelatorio();
    fixture.whenStable().then(() => {
      expect(component.agendamentoForm.get('vrTipoRelatorio')?.value).toBe(
        null
      );
    });
  });

  it('should enable diaSemana when Periodo Semanal is selected', () => {
    const debugElement = fixture.debugElement;
    const matSelect = debugElement.query(By.css('.mat-select-trigger'))
      .nativeElement;

    matSelect.click();
    fixture.detectChanges();
    const matOption = debugElement.queryAll(By.css('.mat-option'))[1]
      .nativeElement;
    matOption.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(
        fixture.nativeElement.querySelector('#diaSemana').disabled
      ).toBeFalse();
      expect(
        fixture.nativeElement.querySelector('#agendamento').disabled
      ).toBeTrue();
    });
  });

  it('should enable agendamento when Periodo Mensal is selected', () => {
    const debugElement = fixture.debugElement;
    const matSelect = debugElement.query(By.css('.mat-select-trigger'))
      .nativeElement;

    matSelect.click();
    fixture.detectChanges();
    const matOption = debugElement.queryAll(By.css('.mat-option'))[2]
      .nativeElement;
    matOption.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(
        fixture.nativeElement.querySelector('#diaSemana').disabled
      ).toBeTrue();
      expect(
        fixture.nativeElement.querySelector('#agendamento').disabled
      ).toBeFalse();
    });
  });

  it('should mark pdfCheckbox when checkbox is clicked', async () => {
    const pdfCheckbox = await loader.getHarness(
      MatCheckboxHarness.with({
        label: 'PDF',
      })
    );

    await pdfCheckbox.check();
    fixture.detectChanges();
    expect(component.pdfCheckbox).toBeTrue();
  });

  it('should mark excelCheckbox when checkbox is clicked', async () => {
    const excelCheckbox = await loader.getHarness(
      MatCheckboxHarness.with({
        label: 'Excel'
      })
    );
    await excelCheckbox.check();
    fixture.detectChanges();
    expect(component.excelCheckbox).toBeTrue();
  });
});
