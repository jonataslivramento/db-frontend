import { ExtratoState } from '../../../redux/extrato.state';
import { ExtratoStore } from '../../../redux/extrato.store';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { IExtratoDerivativo } from '@shared/interfaces/ExtratoDerivativo';
import { DestroyService } from '@shared/services/destroy/destroy.service';
import { ExtratoService } from '@shared/services/extrato/extrato.service';
import { takeUntil } from 'rxjs/operators';
import {
  colunasConsultaExtratoDerivativoDialog,
  itensPorPagina,
} from '../../../../shared/constants/index';
import { GridDerivativoManutencaoParameters } from '../../../../shared/interfaces/GridDerivativoManutencaoParameters';

@Component({
  selector: 'app-extrato-dialog-list',
  templateUrl: './extrato-dialog-list.component.html',
  styleUrls: ['./extrato-dialog-list.component.scss'],
  providers: [DestroyService],
})
export class ExtratoDialogListComponent
  implements OnInit, OnChanges, AfterViewInit {
  @Input() search: string;
  @Output() selectEmitter: EventEmitter<any> = new EventEmitter();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  totalElements: number;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions = itensPorPagina;
  extratos: IExtratoDerivativo[] = [];

  displayedColumns: string[] = colunasConsultaExtratoDerivativoDialog;
  dataSource: MatTableDataSource<IExtratoDerivativo>;
  selection = new SelectionModel<IExtratoDerivativo>(true, []);

  constructor(
    private extratoService: ExtratoService,
    private destroy$: DestroyService,
    private store: ExtratoStore,
    public dialog: MatDialog
  ) {
    this.dataSource = new MatTableDataSource<any>(this.extratos);
  }

  ngOnInit(): void {
    this.getExtratoDerivativo();
    this.store.state$
      .pipe(takeUntil(this.destroy$))
      .subscribe((state: ExtratoState) => {
        if (state.update) {
          this.refreshExtratoDerivativo();
        }
      });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (data, filter: string): boolean => {
      return data.nomeCliente.toLowerCase().includes(filter);
    };
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.search) {
      const search = changes.search.currentValue.trim().toLowerCase();
      this.dataSource.filter = search;
    }
  }

  getExtratoDerivativo(): void {
    const parameters: GridDerivativoManutencaoParameters = {
      page: this.pageIndex.toLocaleString().replace(/,/g, ''),
      size: this.pageSize.toLocaleString(),
    };
    this.extratoService
      .getExtratoDerivativo(parameters)
      .pipe(takeUntil(this.destroy$))
      .subscribe((response: any) => {
        this.extratos = response.content;
        this.selection.clear();
        this.totalElements = response.totalElements;
        this.updateDataSource();
      });
  }

  refreshExtratoDerivativo(): void {
    if (this.pageIndex !== 0) {
      this.pageIndex = 0;
      this.paginator.firstPage();
    } else {
      this.getExtratoDerivativo();
    }
  }

  updateDataSource(): void {
    this.dataSource.data = this.extratos;
    this.selection = new SelectionModel<any>(true, []);
  }

  handleChangePage(pageEvent: PageEvent): void {
    this.pageIndex = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;

    this.getExtratoDerivativo();
  }

  checkboxLabel(row?: any): string {
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.nomeCliente
    }`;
  }

  handleChange(clienteManutencao: IExtratoDerivativo): void {
    this.selection.toggle(clienteManutencao);
    this.selectEmitter.emit(this.selection.selected);
  }

  get dialogDisabled(): boolean {
    return this.selection.selected.length === 0;
  }

  disableToggle(clienteManutencao: IExtratoDerivativo): boolean {
    const clientesSelecionados = this.selection.selected;
    if (clientesSelecionados.length === 0) {
      return false;
    } else if (
      clientesSelecionados.every((cliente) => cliente.isInsertedOnExtract) &&
      clienteManutencao.isInsertedOnExtract
    ) {
      return false;
    } else if (
      clientesSelecionados.every((cliente) => !cliente.isInsertedOnExtract) &&
      !clienteManutencao.isInsertedOnExtract
    ) {
      return false;
    } else {
      return true;
    }
  }
}
