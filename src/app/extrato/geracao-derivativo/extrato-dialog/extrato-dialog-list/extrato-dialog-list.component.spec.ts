import { IExtratoDerivativo } from '@shared/interfaces/ExtratoDerivativo';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ExtratoDialogListComponent } from './extrato-dialog-list.component';

const extratosDerivativos: IExtratoDerivativo[] = [
  {
    isInsertedOnExtract: false,
    cdCliente: 1,
    nomeCliente: 'Claudia',
    cpfCnpjCliente: '32477387000168',
    vrAgendamento: '',
    vrPeriodo: 'MENSAL',
    vrHorario: 'MANHA',
    vrDiaSemana: 'SEGUNDA',
    dtRef: '28',
    vrTipoRelatorio: 'PDF',
    vrEmailFlag: 'S',
    cdSitPessoa: 'A',
  },
  {
    isInsertedOnExtract: false,
    cdCliente: 2,
    nomeCliente: 'Marcio',
    cpfCnpjCliente: '32477387000168',
    vrAgendamento: '',
    vrPeriodo: 'MENSAL',
    vrHorario: 'MANHA',
    vrDiaSemana: 'SEGUNDA',
    dtRef: '28',
    vrTipoRelatorio: 'PDF',
    vrEmailFlag: 'S',
    cdSitPessoa: 'A',
  },
];

describe('ExtratoDialogListComponent', () => {
  let component: ExtratoDialogListComponent;
  let fixture: ComponentFixture<ExtratoDialogListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule, HttpClientTestingModule],
      declarations: [ExtratoDialogListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtratoDialogListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should update search text with 'Cliente'`, () => {
    const search = 'Cliente';
    component.ngOnChanges({
      search: new SimpleChange(null, search, true),
    });
    fixture.detectChanges();
    expect(component.dataSource.filter).toBe(search.toLowerCase());
  });

  it(`should start with dialog disabled`, () => {
    expect(component.dialogDisabled).toBeTrue();
  });

  it(`should return name of cliente`, () => {
    expect(component.dialogDisabled).toBeTrue();
  });

  it(
    `should add checkbox label to select a row`,
    waitForAsync(() => {
      component.ngOnInit();
      const checkboxLabel = component.checkboxLabel(extratosDerivativos[0]);
      fixture.whenStable().then(() => {
        expect(checkboxLabel).toBe(`select row ${extratosDerivativos[0].nomeCliente}`);
      });
    })
  );

  it(
    `should add checkbox label to deselect a row`,
    waitForAsync(() => {
      component.selection.select(extratosDerivativos[0]);
      const checkboxLabel = component.checkboxLabel(extratosDerivativos[0]);
      fixture.whenStable().then(() => {
        expect(checkboxLabel).toBe(`deselect row ${extratosDerivativos[0].nomeCliente}`);
      });
    })
  );


  it(`should emit a new selection when checkbox is changed`, () => {
    spyOn(component.selectEmitter, 'emit');
    component.handleChange(extratosDerivativos[0]);
    fixture.detectChanges();
    expect(component.selectEmitter.emit).toHaveBeenCalled();
  });

  it(
    `should disable rows without 'adicionado' property when one with it has been selected`,
    waitForAsync(() => {
      component.ngOnInit();
      component.dataSource = new MatTableDataSource<IExtratoDerivativo>(
        extratosDerivativos
      );

      component.handleChange(extratosDerivativos[1]);
      fixture.whenStable().then(() => {
        const clienteDisabled = component.disableToggle(extratosDerivativos[0]);
        expect(clienteDisabled).toBeFalse();
      });
    })
  );

  it(
    `should disable rows with 'adicionado' property when one without it has been selected`,
    waitForAsync(() => {
      component.ngOnInit();
      component.dataSource = new MatTableDataSource<IExtratoDerivativo>(
        extratosDerivativos
      );

      component.handleChange(extratosDerivativos[0]);
      fixture.whenStable().then(() => {
        const clienteDisabled = component.disableToggle(extratosDerivativos[1]);
        expect(clienteDisabled).toBeFalse();
      });
    })
  );
});
