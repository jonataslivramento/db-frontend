import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IExtratoDerivativo } from '../../../shared/interfaces/ExtratoDerivativo';

@Component({
  templateUrl: './extrato-response-dialog.component.html',
  styleUrls: ['./extrato-response-dialog.component.scss'],
})
export class ExtratoResponseDialogComponent implements OnInit {
  clientDTOs: any = [];
  msgErros: any = [];
  extratos: IExtratoDerivativo[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ExtratoResponseDialogComponent>
  ) {}

  ngOnInit(): void {
    this.clientDTOs = this.data.response
      .filter((response: any) => response.clientDTOs.length > 0)
      .map((filteredResponse: any) => filteredResponse.clientDTOs[0]);

    this.msgErros = this.data.response
      .filter((response: any) => response.msgErros.length > 0)
      .map((filteredResponse: any) => filteredResponse.msgErros);
  }

  close(): void {
    this.dialogRef.close();
  }
}
