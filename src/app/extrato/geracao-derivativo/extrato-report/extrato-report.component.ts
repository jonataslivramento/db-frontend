import { DialogService } from './../../../shared/services/dialog/dialog.service';
import { ExtratoDerivativoReportParameters } from './../../../shared/interfaces/ExtratoDerivativoReportParameters';
import { Component, OnInit } from '@angular/core';
import { DestroyService } from '@shared/services/destroy/destroy.service';
import { ExtratoService } from '@shared/services/extrato/extrato.service';
import { SnackbarService } from '@shared/services/snackbar/snackbar.service';
import { UtilsService } from '@shared/services/utils/utils.service';
import { catchError, take, takeUntil } from 'rxjs/operators';
import { IExtratoDerivativo } from '../../../shared/interfaces/ExtratoDerivativo';
import { ExtratoStore } from '../../redux/extrato.store';
import { Observable, of } from 'rxjs';
import { TextDecoder } from 'text-encoding';

@Component({
  selector: 'app-extrato-report',
  templateUrl: './extrato-report.component.html',
  styleUrls: ['./extrato-report.component.scss'],
  providers: [DestroyService],
})
export class ExtratoReportComponent implements OnInit {
  pdfCheckbox = false;
  excelCheckbox = false;
  mailCheckbox = false;
  dataPosicao = new Date();
  dataAtual = new Date();
  extratos: IExtratoDerivativo[] = [];

  constructor(
    private store: ExtratoStore,
    private destroy$: DestroyService,
    private extratoService: ExtratoService,
    private util: UtilsService,
    private snackbar: SnackbarService,
    private dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.store.state$.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      this.extratos = data.extratos;
    });
  }

  generateReport(): void {
    if (this.mailCheckbox) {
      this.sendMailReport();
    } else {
      this.downloadZipReport();
    }
  }

  sendMailReport(): void {
    const parameters = {
      data: `${this.dataPosicao.getFullYear()}-${
        this.dataPosicao.getMonth() + 1
      }-${this.dataPosicao.getDate()}`,
      cdClientes: this.extratos
        .map((extrato) => extrato.cdCliente.toString())
        .toString(),
    };

    this.extratoService
      .sendMailReport(parameters)
      .pipe(
        take(1),
        catchError((e) => this.handleError(e))
      )
      .subscribe(() => {
        this.dialogService.openSuccessDialog({
          titulo: 'Relatórios salvos via e-mail com sucesso',
        });
      });
  }
  downloadZipReport(): void {
    const parameters: ExtratoDerivativoReportParameters = {
      excel: this.excelCheckbox,
      pdf: this.pdfCheckbox,
      data: `${this.dataPosicao.getFullYear()}-${
        this.dataPosicao.getMonth() + 1
      }-${this.dataPosicao.getDate()}`,
      cdClientes: this.extratos
        .map((extrato) => extrato.cdCliente.toString())
        .toString(),
    };

    this.extratoService
      .generateReport(parameters)
      .pipe(
        take(1),
        catchError((e) => this.handleError(e))
      )
      .subscribe((response: any) => {
        this.util.onDownloadFile(response, 'application/zip');
      });
  }

  get isReportDisabled(): boolean {
    if (
      (this.mailCheckbox || this.excelCheckbox || this.pdfCheckbox) &&
      this.extratos.length > 0
    ) {
      return false;
    }
    return true;
  }

  get isMailReportSelected(): boolean {
    return this.mailCheckbox;
  }

  get isNotMailReportSelected(): boolean {
    return this.excelCheckbox || this.pdfCheckbox;
  }

  handleError(error: any): Observable<any> {
    if (!window['TextDecoder']) {
      window['TextDecoder'] = TextDecoder;
    }
    const enc = new TextDecoder();
    console.log(error);
    if (error.error.constructor === ArrayBuffer) {
      const e = JSON.parse(enc.decode(error.error));
      this.snackbar.showSnackbarError(e.message);
    } else {
      this.snackbar.showSnackbarError(error.message);
    }
    return of();
  }
}
