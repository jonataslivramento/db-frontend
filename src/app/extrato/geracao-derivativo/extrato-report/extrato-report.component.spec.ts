import { MatSnackBarModule } from '@angular/material/snack-bar';
import { UtilsService } from '@shared/services/utils/utils.service';
import { SnackbarService } from '@shared/services/snackbar/snackbar.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtratoReportComponent } from './extrato-report.component';

describe('ExtratoReportComponent', () => {
  let component: ExtratoReportComponent;
  let fixture: ComponentFixture<ExtratoReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatSnackBarModule],
      providers: [SnackbarService, UtilsService],
      declarations: [ExtratoReportComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtratoReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
