import {
  HORARIO_AGENDAMENTO_ENUM,
  HORARIO_AGENDAMENTO_HORARIO_ENUM,
} from './../../constants/agendamento';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'horarioAgendamento',
})
export class HorarioAgendamentoPipe implements PipeTransform {
  transform(value: string): string {
    if (!value) {
      return '';
    }

    if (value === HORARIO_AGENDAMENTO_ENUM.MANHA) {
      return HORARIO_AGENDAMENTO_HORARIO_ENUM.MANHA;
    } else if (value === HORARIO_AGENDAMENTO_ENUM.TARDE) {
      return HORARIO_AGENDAMENTO_HORARIO_ENUM.TARDE;
    } else if (value === HORARIO_AGENDAMENTO_ENUM.NOITE) {
      return HORARIO_AGENDAMENTO_HORARIO_ENUM.NOITE;
    } else {
      return value;
    }
  }
}
