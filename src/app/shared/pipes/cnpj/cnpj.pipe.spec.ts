import { CnpjPipe } from './cnpj.pipe';

describe('CnpjPipe', () => {
  const pipe = new CnpjPipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform "74842960000135" in "74.842.960/0001-35"', () => {

    expect(pipe.transform('74842960000135')).toBe('74.842.960/0001-35');

  });
});
