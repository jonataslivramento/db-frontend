import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'modalidadePipe',
})
export class ModalidadePipe implements PipeTransform {
  transform(value: string | number): string {
    switch (value) {
      case 1:
        return 'Parametrizada';
      case 2:
        return 'Não Parametrizada';

      default:
        return '';
    }
  }
}
