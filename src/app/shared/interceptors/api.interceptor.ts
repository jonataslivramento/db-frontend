import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  constructor(private router: Router) {}
  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const authToken = window.localStorage.getItem('token');
    const { url } = request;

    let newRequest = request.clone({
      setHeaders: {
        Pragma: 'no-cache',
        Expires: 'Sat, 01 Jan 2000 00:00:00 GMT',
      },
      url: `${environment.apiUrl}${url}`,
    });

    if (authToken) {
      newRequest = newRequest.clone({
        setHeaders: {
          Authorization: `Bearer ${authToken}`,
        },
      });
    }

    return next
      .handle(newRequest)
      .pipe(catchError((error) => this.errorHandler(error)));
  }

  private errorHandler(response: any | null): Observable<HttpEvent<any>> {
    switch (response.status) {
      case 401:
        this.router.navigate(['/login']);
        break;

      default:
        break;
    }

    throw response;
  }
}
