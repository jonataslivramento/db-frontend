import { TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { ApiInterceptor } from './api.interceptor';
import { ExtratoService } from '@shared/services/extrato/extrato.service';
import { environment } from 'src/environments/environment';

describe('ApiInterceptor', () => {
  let service: ExtratoService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ApiInterceptor,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ApiInterceptor,
          multi: true,
        },
      ]
    });

    service = TestBed.inject(ExtratoService);
    httpMock = TestBed.inject(HttpTestingController);
  });


  it('should add url prefixe', () => {
    service.getConsolidadeAndAnlyticalExtractPdf(0, 0, 0, '', '', '', '')
      .subscribe(response => expect(response).toBeTruthy());

    const httpRequest = httpMock.expectOne(`${environment.apiUrl}//?cdEmpresa=0&cdCliente=0&cdModalidade=0&sort=&dtReferencia=`);

    expect(httpRequest.request.url).toContain('http');
  });
});
