export const ORIGEM_MIDAS = [
  { key: 'TODOS', value: '' },
  { key: 'VCTB: Valor Contabil', value: 'VCTB' },
  { key: 'VPFI: Valor Principal Currency do Financeiro', value: 'VPFI' },
  { key: 'VPOP: Valor Principal Currency da Operacao', value: 'VPOP' },
  { key: 'VPEM: Valor Principal Empresa', value: 'VPEM' },
  {
    key: 'SDPF: Saldo Devedor Principal Currency do Financeiro',
    value: 'SDPF',
  },
  { key: 'SDPO: Saldo Devedor Principal Currency da Operação', value: 'SDPO' },
  { key: 'SDPE: Saldo Devedor Principal Currency da Empresa', value: 'SDPE' },
  { key: 'SDJF: Saldo Devedor Juros Currency do Financeiro', value: 'SDJF' },
  { key: 'SDJO: Saldo Devedor Juros Currency da Operação', value: 'SDJO' },
  { key: 'QTDE: Quantidade Total da Operação', value: 'QTDE' },
];

export const DESTINO_EXTRATO = [
  { key: 'TODOS', value: '' },
  { key: 'QTDE: Quantidade Total da Operação', value: 'QTDE' },
  { key: 'VCTB: Valor Contábil', value: 'VCTB' },
  { key: 'VCME: Valor Corrigido na Currency Estrangeira', value: 'VCME' },
  { key: 'VOME: Valor da Operação na Currency Estrangeira', value: 'VOME' },
  { key: 'VOMN: Valor da Operação na Currency Nacional', value: 'VOMN' },
  { key: 'VOPE: valor da Operação na Currency da Empresa', value: 'VOPE' },
  { key: 'VJME: Valor de Juros na Currency Estrangeira', value: 'VJME' },
  { key: 'VJMN: Valor de Juros na Currency Nacional', value: 'VJMN' },
];

export const colunasPreMontagemManutencao = [
  'select',
  'cdSeqModAplic',
  'cdApplication',
  'cdModalidade',
  'dsNegocio',
  'dsNegocioIngles',
  'displayQtde',
  'displayMe',
  'displayMn',
  'iiTipoContabil',
  'dsObn',
  'cgSistema',
  'cgModulo',
  'nmIndexador',
  'cdGarantia',
];

export const colunasPreMontagemParametrizacao = [
  'select',
  'cdSeqModalidade',
  'cdModalidade',
  'cdSisOrig',
  'qtCasaDec',
  'iddDTrro',
  'iddDCamd',
  'iddDCaex',
  'iddDSnop',
  'cdProduto',
  'cdMoedaOper'
];
