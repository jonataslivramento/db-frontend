export const itensPorPagina: number[] = [5, 10, 20, 50];
export const snackbarValue = {
  duration: 3000,
  horizontalPosition: 'right',
  verticalPosition: 'top',
};
export const colunasConsultaConsolidadaEAnalitica: string[] = [
  'cdCliente',
  'nmCliente',
  'dtReferencia',
  'dsNegocio',
  'cdProduto',
  'cdModalidade',
  'dtInicioOper',
  'dtVencimentoOper',
  'moeda',
  'qtde',
  'displayQtde',
  'sgModulo',
  'cdCgcCpf',
  'dcEnd1',
];
export const colunasConsultaExtratoDerivativoDialog = [
  'select',
  'isInsertedOnExtract',
  'cdCliente',
  'nomeCliente',
  'cpfCnpjCliente',
  'vrPeriodo',
  'vrHorario',
  'vrDiaSemana',
  'dtRef',
  'vrTipoRelatorio',
  'vrEmailFlag',
];
export const colunasPosMontagem = ['select', ...colunasConsultaConsolidadaEAnalitica]
export const colunasLogDerivativo = [
  'logId',
  'cdCli',
  'nmCli',
  'agTipo',
  'execucao',
  'status',
  'descricao',
  'filename',
  'dtLog',
];
export const colunasAutorizacao = [
  'select',
  'autCdSequencial',
  'autDsTipoOperacao',
  'autStatus',
  'autCdUsuarioSolicitacao',
  'autDtSolicitacao',
  'autCdUsuarioAutorizacao',
  'autDtAutorizacao',
  ...colunasConsultaConsolidadaEAnalitica.filter(item => item !== 'displayQtde')
];
export const removeEmpty = (obj: any) => {
  Object.keys(obj).forEach((k) => (!obj[k] && obj[k] !== undefined) && delete obj[k]);
  return obj;
};
