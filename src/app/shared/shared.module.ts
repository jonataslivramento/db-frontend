import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DateAdapter, MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import {
  MatPaginatorIntl,
  MatPaginatorModule,
} from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import {
  MatSnackBarModule,
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
} from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { snackbarValue } from '@shared/constants';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { SuccessDialogComponent } from './components/success-dialog/success-dialog.component';
import { CurrencyInputMaskDirective } from './directives/currency-mask/currency-mask.directive';
import { OnlyIntegerNumberDirective } from './directives/only-integer-number/only-integer-number.directive';
import { BrazilianDateAdapter } from './helpers/date.adapter';
import { ApiInterceptor } from './interceptors/api.interceptor';
import { CnpjPipe } from './pipes/cnpj/cnpj.pipe';
import { HorarioAgendamentoPipe } from './pipes/horario-agendamento/horario-agendamento.pipe';
import { ModalidadePipe } from './pipes/modalidade/modalide.pipe';
import { CustomPaginatorService } from './services/custom-paginator/custom-paginator.service';

const MATERIAL_MODULES = [
  MatCardModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatCheckboxModule,
  MatTableModule,
  MatDatepickerModule,
  MatPaginatorModule,
  MatRadioModule,
  MatButtonToggleModule,
  MatFormFieldModule,
  MatSelectModule,
  MatProgressSpinnerModule,
  MatSnackBarModule,
  MatMenuModule,
  MatSortModule,
  MatStepperModule,
  MatTooltipModule,
];

@NgModule({
  declarations: [
    MainLayoutComponent,
    CnpjPipe,
    ModalidadePipe,
    ConfirmationDialogComponent,
    SuccessDialogComponent,
    SpinnerComponent,
    CurrencyInputMaskDirective,
    HorarioAgendamentoPipe,
    OnlyIntegerNumberDirective,
  ],
  imports: [
    CommonModule,
    MatNativeDateModule,
    RouterModule,
    HttpClientModule,
    ...MATERIAL_MODULES,
  ],
  exports: [
    ...MATERIAL_MODULES,
    MainLayoutComponent,
    CnpjPipe,
    ModalidadePipe,
    SpinnerComponent,
    CurrencyInputMaskDirective,
    HorarioAgendamentoPipe,
    OnlyIntegerNumberDirective,
  ],
  providers: [
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: snackbarValue },
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
    { provide: MatPaginatorIntl, useClass: CustomPaginatorService },
    { provide: DateAdapter, useClass: BrazilianDateAdapter },
  ],
})
export class SharedModule {}
