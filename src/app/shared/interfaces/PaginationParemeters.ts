export interface PaginationParameters {
  page?: string;
  size?: string;
}
