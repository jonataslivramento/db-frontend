export interface IPreMontagemManutencao {
  cdApplication: number;
  cdModalidade: number;
  cdGarantia: string;
  cdSeqModAplic: number;
  cdSisOrig: string;
  cgModulo: string;
  cgSistema: string;
  displayMe: string;
  displayMn: string;
  displayQtde: string;
  dsNegocio: string;
  dsNegocioIngles: string;
  dsObn: string;
  iiTipoContabil: string;
  modalidade: string;
  nmIndexador: string;
}
