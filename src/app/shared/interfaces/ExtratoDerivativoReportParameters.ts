export interface ExtratoDerivativoReportParameters {
  data: string;
  cdClientes: string; // separados por vírgula
  pdf: boolean;
  excel: boolean;
}
