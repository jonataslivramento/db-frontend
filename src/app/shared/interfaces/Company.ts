export interface Company {
  cdEmpresa: number,
  nomeEmpresa: string,
  cdGrupoEmpresa: string,
  nmGrupoEmpresa: string,
  cdHoldingEmpresa: string,
  nmHoldingEmpres: string,
  cdMoedaEmp: number,
  cdTpemp: string,
  cdFLuxoCaixa: number,
  nmFluxoCaixa: string,
  dbCons: number,
  dmpCode: number
}