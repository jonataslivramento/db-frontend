export interface IPreMontagemParametrizacao {
  cdModalidade: number;
  cdMoedaOper: string;
  cdProduto: string;
  cdSeqModalidade: number;
  cdSisOrig: string;
  idDCaex: string;
  idDCamd: string;
  idDSnop: string;
  idDTrro: string;
  iddDCaex: string;
  iddDCamd: string;
  iddDSnop: string;
  iddDTrro: string;
  qtCasaDec: number;
}
