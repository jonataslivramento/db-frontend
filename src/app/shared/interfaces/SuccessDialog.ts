import { IFormatOptions } from './FormatOptions';
export interface ISuccessDialog {
  titulo: string;
  dados?: any[];
  acao?: string;
  format?: IFormatOptions;
}
