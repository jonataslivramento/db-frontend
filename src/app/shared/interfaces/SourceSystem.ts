export interface SourceSystem {
  cdSeqSisOrig: number;
  cdSisOrig: string;
  nmSistema: string;
}
