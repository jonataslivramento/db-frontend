export interface Currency {
  cdMoeda: number;
  cdMoedaIso: string;
  nmMoeda: string;
  tpParidadeDolar: string;
  dtExclusaoPtax: Date;
}
