export interface IFormatOptions {
  key: string;
  value: string;
  separator?: string;
}
