export interface ConsolidatedAndAnalyticalExtract {
  pageable: {
    sort: {
      sorted: any;
      unsorted: any;
      empty: any;
    },
    offset: any;
    pageNumber: any;
    pageSize: any;
    paged: any;
    unpaged: any
  };
  totalElements: any;
  totalPages: any;
  last: any;
  size: any;
  number: any;
  sort: {
    sorted: any;
    unsorted: any;
    empty: any
  };
  numberOfElements: any;
  first: any;
  empty: any;
  content: ConsolidateAndAnlytical[];
}

export interface ConsolidateAndAnlytical {
  cdEmpresa: any;
  cdCliente: any;
  cdModalidade: any;
  dsNegocio: any;
  dsNegocioIngles: any;
  cdOperOrigem: any;
  cdContrato: any;
  dtReferencia: any;
  dtInicioOper: any;
  dtVencimentoOper: any;
  dtLiquidacaoOper: any;
  cdGrprod: any;
  cdFamprod: any;
  cdProduto: any;
  sgSistema: any;
  sgModulo: any;
  cdMoeda: any;
  qtde: any;
  vlContabil: any;
  vlCorrigidoMe: any;
  vlOperacaoMe: any;
  vlOperacaoMn: any;
  vlOperacaoEmp: any;
  vlJurosMe: any;
  vlJurosMn: any;
  iiTipoContabil: any;
  cdCgcCpf: any;
  nmCliente: any;
  dcEnd1: any;
  dcEnd2: any;
  dcEnd3: any;
  cdPais: any;
  nomeAosCuidados: any;
  dsObn: any;
  nmEmpresa: any;
  cdFilial: any;
  nmIndexador: any;
  cdGarantia: any;
  cdModulo: any,
  moeda: any;
  dsProduto: any;
}
