export interface IExtratoDerivativo {
  isInsertedOnExtract: boolean;
  cdCliente: number;
  nomeCliente: string;
  cpfCnpjCliente: string;
  vrAgendamento: string;
  vrPeriodo: string;
  vrHorario: string;
  vrDiaSemana: string;
  dtRef: string;
  vrTipoRelatorio: string;
  vrEmailFlag: string;
  cdSitPessoa: string;
}
