export interface Indexer {
  cdIndexador: number;
  nmIndexador: string;
  flTipoInx: string;
  cdMoedaDe: number;
  cdMoedaPara: number;
  flTipoParidade: string;
  cdTppe: string;
  flTipoParidadeMtm: string;
  cdInxCeri: number;
}
