export interface Modality {
  cdSeqModalidade: number,
  cdModalidade: number,
  dsModalidade: string,
  cdProduto: number,
  dsProduto: string,
  cdGrProduto: number,
  dsGrProduto: string,
  cdFamProduto: number,
  dsFamProduto: string
}