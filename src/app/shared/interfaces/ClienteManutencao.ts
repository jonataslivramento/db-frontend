import { FREQUENCIAS_ENUM } from "@shared/constants/agendamento";

export interface IClienteManutencao {
  adicionado: boolean;
  cdCliente: number;
  nome: string;
  cnpj: number | string;
  periodo?: string | FREQUENCIAS_ENUM;
  horario?: string;
  diaSemana?: string;
  diaMes?: string | number;
  tipoRelatorio?: string;
  enviarEmail?: boolean;
}
