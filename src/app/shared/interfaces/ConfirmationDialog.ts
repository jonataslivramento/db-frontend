import { IFormatOptions } from './FormatOptions';
export interface IConfirmationDialog {
  titulo: string;
  dados?: any[];
  acao: string;
  format?: IFormatOptions;
}
