export interface ILogDerivativo {
  logId: number;
  cdCli: number;
  nmCli: string;
  agTipo: string;
  execucao: string;
  status: string;
  descricao: string;
  filename: string;
  dtLog: Date;
}
