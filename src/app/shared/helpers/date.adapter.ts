import { Injectable } from '@angular/core';
import { NativeDateAdapter } from '@angular/material/core';

@Injectable()
export class BrazilianDateAdapter extends NativeDateAdapter {
  parse(value: string): Date {
    const date = value.split('/');
    if (date.length === 3) {
      return new Date(+date[2], +date[1] - 1, +date[0], 12);
    }
    return new Date();
  }

  format(date: Date, displayFormat: Object): string {
    return (
      ('0' + date.getDate()).slice(-2) +
      '/' +
      ('0' + (date.getMonth() + 1)).slice(-2) +
      '/' +
      date.getFullYear()
    );
  }
}
