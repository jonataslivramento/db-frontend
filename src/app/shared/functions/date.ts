export function dateObjectToDateENUS(date: Date): string {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();

  return `${year}-${month < 10 ? `0${month}` : month}-${day < 10 ? `0${day}` : day}`;
}

export function dateObjectToDateBRL(date: Date): string {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();

  return `${day < 10 ? `0${day}` : day}/${month < 10 ? `0${month}` : month}/${year}`;
}

export function dateENUSToDateObject(value: string): Date {
  const values = value.split('-');

  return new Date(Number(values[0]), Number(values[1]) - 1, Number(values[2]));
}

export function converDateTimeStringToDateTimeBRString(value: string): string {
  if (!value) return '';

  const dateAndTime = value.split(' ');
  const date = dateObjectToDateBRL(dateENUSToDateObject(dateAndTime[0]));
  const time = dateAndTime[1];
  let result = date;

  if (time) {
    result = result.concat(` ${time}`);
  }

  return result;
}
