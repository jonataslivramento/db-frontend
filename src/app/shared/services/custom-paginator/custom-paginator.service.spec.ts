import { TestBed } from '@angular/core/testing';
import { CustomPaginatorService } from './custom-paginator.service';

describe('CustomPaginatorService', () => {
  let service: CustomPaginatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomPaginatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return the text "0 of 25"', () => {
    const text: string = service.getRangeLabel(0, 5, 25);

    expect(text).toEqual('1 - 5 de 25');
  });

  it('should return the text "0 of 0"', () => {
    const text: string = service.getRangeLabel(0, 0, 0);

    expect(text).toEqual('0 de 0');
  });
});
