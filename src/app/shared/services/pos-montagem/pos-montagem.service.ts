import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { dateObjectToDateENUS } from '../../functions/date';

@Injectable({
  providedIn: 'root'
})
export class PosMontagemService {

  constructor(private http: HttpClient) { }

  getListClients(
    page: number,
    size: number,
    cdEmpresa?: number | string,
    cdCliente?: number | string,
    cdModalidade?: number | string
  ): Observable<any> {
    let url = `/extract/FULL/ANALYTIC?page=${page}&size=${size}`;

    if (cdEmpresa !== null && cdEmpresa !== '') {
      url = url.concat(`&cdEmpresa=${cdEmpresa}`);
    }

    if (cdCliente !== null && cdCliente !== '') {
      url = url.concat(`&cdCliente=${cdCliente}`);
    }

    if (cdModalidade !== null && cdModalidade !== 'all' && cdModalidade !== '') {
      url = url.concat(`&cdModalidade=${cdModalidade}`);
    }

    return this.http.get(url);
  }

  getCurrency(id?: number | string): Observable<any> {
    let url = '/currency';

    if (id !== '' && id !== null && id !== undefined) {
      url = url.concat(`/${id}`);
    }

    return this.http.get(url);
  }

  getIndexers(): Observable<any> {
    return this.http.get('/indexer');
  }

  saveMaintenance(values: any): Observable<any> {
    return this.http.post('/extract/aut', this.preparateParams(values));
  }

  updateMaintenance(values: any): Observable<any> {
    return this.http.put(`/extract/aut/${values.oldCodigoEmpresa}/${values.oldCodigoCliente}/${values.oldCodigoModalidade}/${values.oldCodigoOperacaoOrigem}`, this.preparateParams(values));
  }

  deleteMaintenance(
    codigoEmpresa: number,
    codigoCliente: number,
    codigoModalidade: number,
    codigoOperacaoOrigem: string
  ): Observable<any> {
    return this.http.delete(`/extract/aut/${codigoEmpresa}/${codigoCliente}/${codigoModalidade}/${codigoOperacaoOrigem}`);
  }

  private preparateParams(values: any): any {
    return {
      cdEmpresa: values.codigoEmpresa,
      cdCliente: values.codigoCliente,
      cdModalidade: values.codigoModalidade,
      cdOperOrigem: values.codigoOperacaoOrigem,
      cdContrato: values.codigoContrato,
      dtReferencia: values.dataReferencia ? dateObjectToDateENUS(values.dataReferencia) : null,
      dtInicioOper: values.dataInicioOperacao ? dateObjectToDateENUS(values.dataInicioOperacao) : null,
      dtVencimentoOper: values.dataVencimentoOperacao ? dateObjectToDateENUS(values.dataVencimentoOperacao) : null,
      dtLiquidacaoOper: values.dataLiquidacaoOperacao ? dateObjectToDateENUS(values.dataLiquidacaoOperacao) : null,
      cdGrprod: values.codigoGrupoProduto,
      cdFamprod: values.codigoFamiliaProduto,
      cdProduto: values.codigoProduto,
      sgSistema: values.siglaSistema,
      sgModulo: values.siglaModulo,
      cdMoeda: values.codigoMoeda,
      quantidade: values.quantidade,
      vlContabil: values.valorContabil,
      vlCorrigidoMe: values.valorCorrigidoMoedaEstrangeira,
      vlOperacaoMe: values.valorOperacaoMoedaEmpresa,
      vlOperacaoMn: values.valorOperacaoMoedaNacional,
      vlOperacaoEmp: values.valorOperacaoMoedaEmpresa,
      vlJurosMe: values.valorJurosMoedaEstrangeira,
      vlJurosMn: values.valorJurosMoedaNacional,
      iiTipoContabil: values.tipoContabil,
      cdCgcCpf: values.cnpjCpf,
      nmCliente: values.nomeCliente,
      dcEnd1: values.endereco1,
      dcEnd2: values.endereco2,
      dcEnd3: values.endereco3,
      cdPais: values.codigoPais,
      nomeAosCuidados: values.nomeAosCuidados,
      dsObn: values.descricaoObjetoNegociacao,
      nmEmpresa: values.nomeEmpresa,
      cdFilial: values.codigoFilial,
      nmIndexador: values.nomeIndexador,
      cdGarantia: values.codigoGarantia
    };
  }

}

