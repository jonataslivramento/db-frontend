import { TestBed } from '@angular/core/testing';

import { PosMontagemService } from './pos-montagem.service';

describe('PosMontagemService', () => {
  let service: PosMontagemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PosMontagemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
