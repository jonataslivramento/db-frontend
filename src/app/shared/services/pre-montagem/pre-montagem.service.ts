import { IPreMontagemManutencao } from './../../interfaces/PreMontagemManutencao';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IPreMontagemParametrizacao } from '@shared/interfaces/PreMontagem';

@Injectable({
  providedIn: 'root',
})
export class PreMontagemService {
  constructor(private http: HttpClient) {}

  getPreMontagemList(parameters: any): Observable<any> {
    return this.http.get(`/extrato/pre/modalidade/grid`, {
      params: { ...parameters },
    });
  }

  addModalidade(
    iPreMontagemManutencaoObject: IPreMontagemManutencao
  ): Observable<any> {
    return this.http.post(
      '/extrato/pre/modalidade/add',
      iPreMontagemManutencaoObject
    );
  }

  updateModalidade(
    iPreMontagemManutencaoObject: IPreMontagemManutencao
  ): Observable<any> {
    return this.http.put(
      `/extrato/pre/modalidade/update/${iPreMontagemManutencaoObject.cdSeqModAplic}`,
      iPreMontagemManutencaoObject
    );
  }

  deleteModalidade(cdSeqModAplic: any): Observable<any> {
    return this.http.delete(`/extrato/pre/modalidade/delete/${cdSeqModAplic}`);
  }

  getPreMontagemParametrizacaoList(parameters: any): Observable<any> {
    return this.http.get(`/parameterModality`, {
      params: { ...parameters },
    });
  }

  addParametrizacao(
    iPreMontagemParametrizacaoObject: IPreMontagemParametrizacao
  ): Observable<any> {
    return this.http.post(
      '/parameterModality',
      iPreMontagemParametrizacaoObject
    );
  }

  updateParametrizacao(
    iPreMontagemParametrizacaoObject: IPreMontagemParametrizacao
  ): Observable<any> {
    return this.http.put(
      `/parameterModality/${iPreMontagemParametrizacaoObject.cdSeqModalidade}`,
      iPreMontagemParametrizacaoObject
    );
  }

  deleteParametrizacao(cdSeqModalidade: any): Observable<any> {
    return this.http.delete(`/parameterModality/${cdSeqModalidade}`);
  }

}
