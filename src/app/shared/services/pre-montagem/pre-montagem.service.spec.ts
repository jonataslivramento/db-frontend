import { TestBed } from '@angular/core/testing';

import { PreMontagemService } from './pre-montagem.service';

describe('PreMontagemService', () => {
  let service: PreMontagemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PreMontagemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
