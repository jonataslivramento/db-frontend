import { IExtratoDerivativo } from '@shared/interfaces/ExtratoDerivativo';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class ExtratoService {
  constructor(private httpClient: HttpClient) {}

  getExtratoDerivativo(parameters: any): Observable<any> {
    return this.httpClient.get(`/client/management/gridderivativomanutencao`, {
      params: { ...parameters },
    });
  }

  getLogDerivativos(parameters: any): Observable<any> {
    return this.httpClient.get(`/extrato/logderivativos`, {
      params: { ...parameters },
    });
  }

  updateExtratos(extratos: IExtratoDerivativo[]): Observable<any> {
    return this.httpClient.post(`/client/management/add`, extratos);
  }

  removerAgendamentos(extratos: IExtratoDerivativo[]): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: extratos,
    };

    return this.httpClient.delete(`/client/management/delete`, options);
  }

  removerExtratoDerivativo(extratos: IExtratoDerivativo[]): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: extratos,
    };

    return this.httpClient.delete(`/client/management/deleteall`, options);
  }

  generateReport(parameters: any): Observable<any> {
    return this.httpClient.get('/derivatives/zip', {
      params: { ...parameters },
      responseType: 'arraybuffer',
    });
  }

  sendMailReport(parameters: any): Observable<any> {
    return this.httpClient.get('/derivatives/email', {
      params: { ...parameters }
    });
  }

  getConsolidadeAndAnlyticalExtract(
    cdEmpresa: number,
    cdCliente: number,
    cdModalidade: number | string,
    tipoRelatorio: string,
    extractType: string,
    pageNumber: number,
    pageSize: number,
    date: string
  ): Observable<any> {
    let url = `/extract/${tipoRelatorio}/${extractType}?page=${pageNumber}&size=${pageSize}&dtReferencia=${date}`;

    if (cdEmpresa !== null) {
      url = url.concat(`&cdEmpresa=${cdEmpresa}`);
    }
    if (cdCliente !== null) {
      url = url.concat(`&cdCliente=${cdCliente}`);
    }
    if (cdModalidade !== null && cdModalidade !== 'all') {
      url = url.concat(`&cdModalidade=${cdModalidade}`);
    }

    return this.httpClient.get(url);
  }

  getConsolidadeAndAnlyticalExtractPdf(
    cdEmpresa: number,
    cdCliente: number,
    cdModalidade: number | string,
    extractType: string,
    type: string,
    ordenacao: string,
    date: string
  ): Observable<any> {
    let url = `/extract/report/${extractType}/${type}/?sort=${ordenacao}&dtReferencia=${date}`;

    if (cdEmpresa !== null) {
      url = url.concat(`&cdEmpresa=${cdEmpresa}`);
    }
    if (cdCliente !== null) {
      url = url.concat(`&cdCliente=${cdCliente}`);
    }
    if (cdModalidade !== null && cdModalidade !== 'all') {
      url = url.concat(`&cdModalidade=${cdModalidade}`);
    }

    return this.httpClient.get(url, { responseType: 'blob' });
  }
}
