import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';

import { ExtratoService } from './extrato.service';

describe('ExtratoService', () => {
  let service: ExtratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(ExtratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should perform donwnalodFile function "getConsolidadeAndAnlyticalExtract"', () => {
    expect(service.getConsolidadeAndAnlyticalExtract(0, 0, 0, '', '1', 0, 0, '')).toBeInstanceOf(Observable);
  });

  it('should perform donwnalodFile function "getConsolidadeAndAnlyticalExtractPdf"', () => {
    expect(service.getConsolidadeAndAnlyticalExtractPdf(0, 0, 0, '1', '2', '3', '4')).toBeInstanceOf(Observable);
  });
});
