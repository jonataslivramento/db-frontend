import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutorizacaoService {

  constructor(private http: HttpClient) { }

  getOperations(status: string, date: string, pageSize: number, pageNumber: number): Observable<any> {
    let url = `/extract/aut?size=${pageSize}&page=${pageNumber}&dtReferencia=${date}`;

    if (status !== null && status !== '') {
      url = url.concat(`&autStatus=${status}`);
    }

    return this.http.get(url);
  }

  rejectOperation(codigoSequencial: number): Observable<any> {
    return this.http.post(`/extract/aut/${codigoSequencial}/rejeitar`, null);
  }

  aproveOperation(codigoSequencial: number): Observable<any> {
    return this.http.post(`/extract/aut/${codigoSequencial}/aprovar`, null);
  }
}
