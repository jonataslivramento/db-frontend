import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ConsultaService {
  constructor(private http: HttpClient) {}

  getCompanies(): Observable<any> {
    return this.http.get('/company');
  }

  getModalities(): Observable<any> {
    return this.http.get('/modality');
  }

  getIndexers(): Observable<any> {
    return this.http.get('/indexer');
  }

  getProducts(): Observable<any> {
    return this.http.get('/modality/prod');
  }

  getSourceSystem(): Observable<any> {
    return this.http.get('/sistemaogirem/all');
  }

  getCurrency(): Observable<any> {
    return this.http.get('/currency');
  }

  getReferenceDate(): Observable<any> {
    return this.http.get('/extract/date-reference');
  }
}
