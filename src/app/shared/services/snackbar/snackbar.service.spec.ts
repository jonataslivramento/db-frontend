import { TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { snackbarValue } from '@shared/constants';

import { SnackbarService } from './snackbar.service';

describe('SnackbarService', () => {
  let service: SnackbarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule, NoopAnimationsModule]
    });
    service = TestBed.inject(SnackbarService);
  });

  it('should appear in the DOM', () => {
    service.showSnackbarError('Test snackbar');

    const element = document.querySelector('.mat-snack-bar-container');
    expect(element).toBeInstanceOf(HTMLElement);
  })
});
