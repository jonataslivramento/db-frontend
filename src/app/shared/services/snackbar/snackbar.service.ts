import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private snackbar: MatSnackBar) { }

  showSnackbarError(message: string): void {
    this.snackbar.open(message, '', { panelClass: 'custom-snackbar-error' });
  }

  showSnackbarInfo(message: string): void {
    this.snackbar.open(message, '', { panelClass: 'custom-snackbar-info' })
  }
}
