import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';

import { ProfileType } from '../../enums/ProfileType';
import { LoginService } from '../login/login.service';
import { SnackbarService } from '../snackbar/snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userName: string;
  private profiles: string[];

  constructor(private loginService: LoginService, private snackbar: SnackbarService) {
    this.userName = window.localStorage.getItem('name') || '';
    this.profiles = [];

    this.checkIfHasProfiles();
  }

  checkIfHasProfiles(): void {
    if (this.profiles.length === 0) {
      this.loginService.getPefiles(this.userName)
        .pipe(take(1))
        .subscribe((response: any) => {
          this.profiles = response.acessos;
        }, err => this.snackbar.showSnackbarError(err.error.message));
    }
  }

  setUserName(text: string): void {
    window.localStorage.setItem('name', text);
    this.userName = text;
  }

  getUserName(): string {
    return this.userName;
  }

  setProfiles(list: string[]): void {
    this.profiles = list;
  }

  getProfiles(): string[] {
    return this.profiles;
  }

  hasPermission(text: string | undefined): boolean {
    if (text === 'midas') {
      return this.profiles.includes(ProfileType.EXTRATO);
    }

    if (text === 'sbs') {
      return this.profiles.includes(ProfileType.DERIVATIVO_LOG) ||
        this.profiles.includes(ProfileType.EXTRATO_DERIVATIVOS);
    }

    return false;
  }
}
