import { Injectable } from '@angular/core';
import { IFormatOptions } from '@shared/interfaces/FormatOptions';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor() {}

  onDownloadFile(blob: any, type: any, filename?: string): void {
    const newBlob = new Blob([blob], { type });

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      if (filename) {
        window.navigator.msSaveOrOpenBlob(newBlob, filename);
      } else {
        window.navigator.msSaveOrOpenBlob(newBlob);
      }
      return;
    }

    const data = window.URL.createObjectURL(newBlob);
    const link = document.createElement('a');
    link.href = data;
    if (filename) {
      link.download = filename;
    }
    link.click();
    setTimeout(() => {
      window.URL.revokeObjectURL(blob);
    }, 100);
  }

  formatDialogData(array: any[], formatOptions: IFormatOptions): string[] {
    if (array) {
      return array.map(
        (data) =>
          `${data[formatOptions.key]} ${formatOptions.separator || '-'} ${
            data[formatOptions.value]
          }`
      );
    }
    return [];
  }
}
