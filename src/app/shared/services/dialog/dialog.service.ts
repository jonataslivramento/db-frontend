import { of, Observable } from 'rxjs';
import { SuccessDialogComponent } from '../../components/success-dialog/success-dialog.component';
import { ISuccessDialog } from '../../interfaces/SuccessDialog';
import { ConfirmationDialogComponent } from '../../components/confirmation-dialog/confirmation-dialog.component';
import { IConfirmationDialog } from '../../interfaces/ConfirmationDialog';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(private dialog: MatDialog) { }

  openConfirmationDialog(confirmationOptions: IConfirmationDialog): Observable<any> {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: confirmationOptions,
      disableClose: true
    });

    return dialogRef.afterClosed();
  }

  openSuccessDialog(successOptions: ISuccessDialog): void {
    this.dialog.open(SuccessDialogComponent, {
      data: successOptions,
    });
  }
}
