import { Component, ViewChild } from '@angular/core';
import {
  ComponentFixture,
  fakeAsync,
  flush,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CurrencyInputMaskDirective } from './currency-mask.directive';

@Component({
  selector: 'app-test-host',
  template: `
    <input
      appCurrencyMask
      [debounceTime]="debounceTime"
      (valueChanged)="valueChanged($event)"
      type="text"
    />
  `,
})
class TestHostComponent {
  @ViewChild(CurrencyInputMaskDirective)
  currencyInputMaskDirective: CurrencyInputMaskDirective;
  public debounceTime = 300;
  valueChanged(newValue: string): void {}
  updateInputValue(value: string | null, savePosition = false): void {}
}

describe('CurrencyInputMaskDirective', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [TestHostComponent, CurrencyInputMaskDirective],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should emit value after keypress', fakeAsync(() => {

    const input = fixture.debugElement.query(By.css('input'));
    input.nativeElement.value = '1';
    input.nativeElement.dispatchEvent(new KeyboardEvent('focus', {}));
    input.nativeElement.dispatchEvent(
      new KeyboardEvent('keypress', {
        bubbles: true,
        cancelable: true,
        key: '1',
        shiftKey: true,
      })
    );

    input.nativeElement.dispatchEvent(new KeyboardEvent('blur', {}));
    flush();

    expect(input.nativeElement.value).toBe('1');
  }));

  it('should emit value after paste', fakeAsync(() => {
    const input = fixture.debugElement.query(By.css('input'));

    input.nativeElement.dispatchEvent(new KeyboardEvent('paste', {}));
    flush();

    expect(input.nativeElement.value).toBe('');
  }));

  it('should emit value after input', fakeAsync(() => {
    const input = fixture.debugElement.query(By.css('input'));

    input.nativeElement.dispatchEvent(new KeyboardEvent('input', {}));
    flush();

    expect(input.nativeElement.value).toBe('');
  }));


  it('should emit value after cut', fakeAsync(() => {
    const input = fixture.debugElement.query(By.css('input'));

    input.nativeElement.dispatchEvent(new KeyboardEvent('cut', {}));
    flush();

    expect(input.nativeElement.value).toBe('');
  }));
});
