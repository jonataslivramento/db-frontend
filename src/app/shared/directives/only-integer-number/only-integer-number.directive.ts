import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appOnlyIntegerNumber]'
})
export class OnlyIntegerNumberDirective {
  @HostListener('input', ['$event']) onInput(event: any): void {
    event.target.value = event.target.value.replace(/\D/g, '');
  }
}
