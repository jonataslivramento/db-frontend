import { OnlyIntegerNumberDirective } from './only-integer-number.directive';

describe('OnlyIntegerNumberDirective', () => {
  it('should only return numbers', () => {
    const directive = new OnlyIntegerNumberDirective();
    const event = { target: { value: 'testando 1234567890' } };

    directive.onInput(event);
    expect(event.target.value).toEqual('1234567890');
  });
});
