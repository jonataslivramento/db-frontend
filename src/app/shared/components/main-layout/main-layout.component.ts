import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, filter } from 'rxjs/operators';

import { BreadCrumb } from '@shared/interfaces/BreadCrumb';
import { UserService } from '@shared/services/user/user.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit, OnDestroy {
  inscricao: Subscription | undefined;
  breadCrumbs: BreadCrumb[];

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
    private userService: UserService) {
    this.breadCrumbs = this.criaBreadCrumb(this.activateRoute.root);
  }

  ngOnInit(): void {
    this.inscricao = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd), distinctUntilChanged())
      .subscribe(() => {
        this.breadCrumbs = this.criaBreadCrumb(this.activateRoute.root);
      });
  }

  ngOnDestroy(): void {
    this.inscricao?.unsubscribe();
  }

  criaBreadCrumb(route: ActivatedRoute, url: string = '', breadcrumbs: BreadCrumb[] = []): BreadCrumb[] {
    let label: string =
      route.routeConfig && route.routeConfig.data
        ? route.routeConfig.data.breadcrumb
        : '';

    let path: string | undefined =
      route.routeConfig && route.routeConfig.data ? route.routeConfig.path : '';

    const lastRoutePart: string | undefined = path?.split('/').pop();
    const isDynamicRoute: boolean | undefined = lastRoutePart?.startsWith(':');

    if (isDynamicRoute && !!route.snapshot) {
      const paramName: string | undefined = lastRoutePart?.split(':')[1];
      path = path?.replace(lastRoutePart || '', route.snapshot.params[paramName || '']);
      label = route.snapshot.params[paramName || ''];
    }

    const nextUrl = path ? `${url}/${path}` : url;

    const breadcrumb: BreadCrumb = {
      label,
      url: nextUrl
    };

    const newBreadcrumbs = breadcrumb.label
      ? [...breadcrumbs, breadcrumb]
      : [...breadcrumbs];

    if (route.firstChild) {
      return this.criaBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
    }

    return newBreadcrumbs;
  }

  logOut(): void {
    window.localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  hasPathInUrl(path: string): boolean {
    return this.router.url.includes(path);
  }

  hasPermission(path: string): boolean {
    return this.userService.hasPermission(path);
  }

  get activeRoute(): string {
    return this.router.url;
  }

  get isLogged(): boolean {
    const token = window.localStorage.getItem('token');

    return token !== null && token !== undefined && token !== '';
  }

  get userName(): string {
    return this.userService.getUserName();
  }

}
