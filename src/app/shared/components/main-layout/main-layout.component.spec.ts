import { MatMenuModule } from '@angular/material/menu';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MainLayoutComponent } from './main-layout.component';

const routes: Routes = [
  { path: '', redirectTo: '/consulta', pathMatch: 'full' },
  {
    path: 'consulta',
    data: { breadcrumb: 'Consulta' },
    loadChildren: () =>
      import('./../../../consulta/consulta.module').then(
        (module) => module.ConsultaModule
      ),
  },
];

describe('MainLayoutComponent', () => {
  let component: MainLayoutComponent;
  let fixture: ComponentFixture<MainLayoutComponent>;
  let route: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes), MatMenuModule],
      declarations: [],
    }).compileComponents();

    route = TestBed.inject(Router);
    route.initialNavigation();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

  it('should update breadcrumb when new route is provided', async () => {
    component.ngOnInit();
    fixture.detectChanges();
    await route.navigate(['/consulta']);
    expect(JSON.stringify(component.breadCrumbs)).toContain('"label":"Consulta"');
  });
});
