import { UtilsService } from '@shared/services/utils/utils.service';
import { ISuccessDialog } from './../../interfaces/SuccessDialog';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './success-dialog.component.html',
  styleUrls: ['./success-dialog.component.scss']
})
export class SuccessDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<SuccessDialogComponent>,
    private utilService: UtilsService,
    @Inject(MAT_DIALOG_DATA) public data: ISuccessDialog
  ) { }

  ngOnInit(): void {
    if (this.data.dados && this.data.format) {
      this.data.dados = this.utilService.formatDialogData(
        this.data.dados,
        this.data.format
      );
    }
  }

}
