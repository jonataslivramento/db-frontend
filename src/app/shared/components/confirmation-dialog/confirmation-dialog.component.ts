import { UtilsService } from '@shared/services/utils/utils.service';
import { IConfirmationDialog } from './../../interfaces/ConfirmationDialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
})
export class ConfirmationDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    private utilService: UtilsService,
    @Inject(MAT_DIALOG_DATA) public data: IConfirmationDialog
  ) { }

  ngOnInit(): void {
    if (this.data.dados && this.data.format) {
      this.data.dados = this.utilService.formatDialogData(
        this.data.dados,
        this.data.format
      );
    }
  }

  close(): void {
    this.dialogRef.close(true);
  }
}
