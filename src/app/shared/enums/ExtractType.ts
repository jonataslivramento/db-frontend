export enum ExtractType {
  ANALYTIC = 'ANALYTIC',
  CONSOLIDATE = 'CONSOLIDATE'
}