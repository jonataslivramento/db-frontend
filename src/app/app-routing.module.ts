import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticGuard } from '@shared/guards/authentic/authentic.guard';
import { PermissionGuard } from '@shared/guards/permission/permission.guard';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: 'midas',
    data: { breadcrumb: 'Midas' },
    canActivate: [AuthenticGuard, PermissionGuard],
    children: [
      {
        path: 'geracao',
        data: { breadcrumb: 'Geração Extrato Consolidada e Analítica' },
        loadChildren: () =>
          import('./consulta/consulta.module').then((m) => m.ConsultaModule),
      },
      {
        path: 'manutencao-modalidade',
        data: { breadcrumb: 'Manutenção de Modalidade' },
        loadChildren: () =>
          import(
            './pre-montagem/manutencao/pre-montagem-manutencao.module'
          ).then((m) => m.PreMontagemManutencaoModule),
      },
      {
        path: 'parametrizacao-modalidade',
        data: { breadcrumb: 'Parametrização de Modalidade' },
        loadChildren: () =>
          import(
            './pre-montagem/parametrizacao/pre-montagem-parametrizacao.module'
          ).then((m) => m.PreMontagemParametrizacaoModule),
      },
      {
        path: 'manutencao-extrato',
        data: { breadcrumb: 'Manutenção Extrato' },
        loadChildren: () =>
          import('./pos-montagem/pos-montagem.module').then(
            (m) => m.PosMontagemModule
          ),
      },
      {
        path: 'autorizacao',
        data: { breadcrumb: 'Autorização Manutenção Extrato' },
        loadChildren: () =>
          import('./autorizacao/autorizacao.module').then(
            (m) => m.AutorizacaoModule
          ),
      },
    ],
  },
  {
    path: 'sbs',
    data: { breadcrumb: 'SBS' },
    canActivate: [AuthenticGuard, PermissionGuard],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./extrato/extrato.module').then((m) => m.ExtratoModule),
      },
    ],
  },
  {
    path: 'login',
    data: { breadcrumb: 'Login' },
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginModule),
  },
  { path: 'autorizacao', loadChildren: () => import('./autorizacao/autorizacao.module').then(m => m.AutorizacaoModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
