import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginService } from '@shared/services/login/login.service';
import { SnackbarService } from '@shared/services/snackbar/snackbar.service';
import { UserService } from '@shared/services/user/user.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  loading: boolean;
  form: FormGroup;

  constructor(
    private loginService: LoginService,
    private snackbar: SnackbarService,
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {
    this.loading = false;
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit(): void {
    const { username, password } = this.form.value;

    this.loading = true;
    this.loginService.login(username, password)
      .pipe(take(1))
      .subscribe(response => {
        window.localStorage.setItem('token', response.token);
        this.userService.setUserName(username);
        this.getPefiles();
      }, error => {
        this.snackbar.showSnackbarError(error.error.message);
        this.loading = false;
      });
  }

  getPefiles(): void {
    this.loginService.getPefiles(this.userService.getUserName())
      .pipe(take(1))
      .subscribe((response: any) => {
        this.userService.setProfiles(response.acessos);
        this.router.navigate(this.userService.hasPermission('midas') ? ['/midas/geracao'] : ['/sbs/geracao']);
      }, err => {
        this.snackbar.showSnackbarError(err.error.message);
        this.loading = false;
      });
  }
}
