import { ListComponent } from './components/list/list.component';
import { Component, ViewChild } from '@angular/core';
import { concatMap, take } from 'rxjs/operators';

import { MatTableDataSource } from '@angular/material/table';
import { PosMontagemService } from '@shared/services/pos-montagem/pos-montagem.service';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { SnackbarService } from '@shared/services/snackbar/snackbar.service';
import { ConsolidateAndAnlytical } from '@shared/interfaces/ConsolidatedAndAnalyticalExtract';
import { FormularioComponent } from './components/formulario/formulario.component';
import { DialogService } from '@shared/services/dialog/dialog.service';
import { idDialogFormulario, initialStateValuesForSubmit } from './constants';

@Component({
  selector: 'app-pos-montagem',
  templateUrl: './pos-montagem.component.html',
  styleUrls: ['./pos-montagem.component.scss'],
})
export class PosMontagemComponent {
  @ViewChild(ListComponent) listComponent: ListComponent;
  dataSouce: MatTableDataSource<ConsolidateAndAnlytical>;
  selection: SelectionModel<any>;
  totalElements: number;
  pageSize: number;
  pageNumber: number;
  valuesForSubmit: any;
  loadingClients: boolean;

  constructor(
    private dialog: MatDialog,
    private posMontagemService: PosMontagemService,
    private snackbar: SnackbarService,
    private dialogService: DialogService
  ) {
    this.dataSouce = new MatTableDataSource<ConsolidateAndAnlytical>([]);
    this.selection = new SelectionModel<any>(false, []);
    this.totalElements = 0;
    this.pageSize = 5;
    this.pageNumber = 0;
    this.loadingClients = false;
    this.valuesForSubmit = initialStateValuesForSubmit;
  }

  setInitialState(): void {
    this.dataSouce = new MatTableDataSource<ConsolidateAndAnlytical>([]);
    this.totalElements = 0;
    this.pageSize = 5;
    this.pageNumber = 0;
  }

  openForm(isEditMode?: boolean): void {
    const itemSelected = this.selection.selected[0];

    this.dialog
      .open(FormularioComponent, {
        width: '95vw',
        maxWidth: '95vw',
        data: {
          oldCodigoModalidade: itemSelected ? itemSelected.cdModalidade : null,
          oldCodigoCliente: itemSelected ? itemSelected.cdCliente : null,
          oldCodigoEmpresa: itemSelected ? itemSelected.cdEmpresa : null,
          oldCodigoOperacaoOrigem: itemSelected
            ? itemSelected.cdOperOrigem
            : null,
          data: itemSelected ? itemSelected : null,
          mode: isEditMode ? 'edit' : 'new',
        },
        disableClose: true,
        id: idDialogFormulario,
      })
      .afterClosed()
      .subscribe(() => this.refresh());
  }

  handleChangePage(event: any): void {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex;
    this.getClientes(
      this.valuesForSubmit.modalidade,
      this.valuesForSubmit.codigoEmpresa,
      this.valuesForSubmit.codigoCliente
    );
  }

  refresh(): void {
    this.getClientes(
      this.valuesForSubmit.modalidade,
      this.valuesForSubmit.codigoEmpresa,
      this.valuesForSubmit.codigoCliente
    );
    this.selection.clear();
  }

  handleSubmit(event: any): void {
    this.valuesForSubmit = event;
    this.listComponent.paginator.firstPage();
    this.getClientes(
      event.modalidade,
      event.codigoEmpresa,
      event.codigoCliente
    );
  }

  onResetForm(): void {
    this.valuesForSubmit = initialStateValuesForSubmit;
    this.selection.clear();
    this.setInitialState();
  }

  changeMatSort(sort: MatSort): void {
    this.dataSouce.sort = sort;
  }

  removeCadastro(): void {
    const {
      cdCliente,
      cdOperOrigem,
      cdEmpresa,
      cdModalidade,
    } = this.selection.selected[0];

    this.dialogService
      .openConfirmationDialog({
        titulo: 'Deseja mesmo remover o registro ?',
        acao: 'Remover',
        dados: [
          { name: 'Cód do Cliente:', data: cdCliente },
          { name: 'Cód da Operação de Origem', data: cdOperOrigem },
        ],
        format: {
          key: 'name',
          value: 'data',
        },
      })
      .pipe(
        concatMap(
          (confirm) =>
            confirm &&
            this.posMontagemService
              .deleteMaintenance(
                cdEmpresa,
                cdCliente,
                cdModalidade,
                cdOperOrigem
              )
              .pipe(take(1))
        )
      )
      .subscribe(
        () => {
          this.dialogService.openSuccessDialog({
            titulo:
              'Registro removido com sucesso! As alterações realizadas têm de ser aprovadas por outro usuário para que elas tenham efeito.',
            acao: 'Ok',
            dados: [
              { name: 'Cód da Operação de Origem', data: cdOperOrigem },
              { name: 'Código Cliente', data: cdCliente },
              { name: 'Código Modalidade', data: cdModalidade },
            ],
            format: {
              key: 'name',
              value: 'data',
            },
          });
          this.dialog.getDialogById(idDialogFormulario)?.close();
        },
        (err) => this.snackbar.showSnackbarError(err.error.message)
      );
  }

  getClientes(
    cdModalidade?: number,
    cdEmpresa?: number,
    cdCLiente?: number
  ): void {
    this.loadingClients = true;
    this.posMontagemService
      .getListClients(
        this.pageNumber,
        this.pageSize,
        cdEmpresa,
        cdCLiente,
        cdModalidade
      )
      .pipe(take(1))
      .subscribe(
        (response) => {
          this.dataSouce = new MatTableDataSource<ConsolidateAndAnlytical>(
            response.content
          );
          this.totalElements = response.totalElements;
          this.loadingClients = false;
        },
        (error) => {
          this.snackbar.showSnackbarError(error.error.message);
          this.setInitialState();
          this.loadingClients = false;
        }
      );
  }
}
