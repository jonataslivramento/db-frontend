import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosMontagemComponent } from './pos-montagem.component';

describe('PosMontagemComponent', () => {
  let component: PosMontagemComponent;
  let fixture: ComponentFixture<PosMontagemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosMontagemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosMontagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
