import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PosMontagemRoutingModule } from './pos-montagem-routing.module';
import { PosMontagemComponent } from './pos-montagem.component';
import { SharedModule } from '@shared/shared.module';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ListComponent } from './components/list/list.component';
import { FormularioComponent } from './components/formulario/formulario.component';
@NgModule({
  declarations: [PosMontagemComponent, SearchFormComponent, ListComponent, FormularioComponent],
  imports: [
    CommonModule,
    PosMontagemRoutingModule,
    SharedModule,
    ReactiveFormsModule,
  ]
})
export class PosMontagemModule { }
