import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { concatMap, take } from 'rxjs/operators';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { dateENUSToDateObject } from '@shared/functions/date';
import { SnackbarService } from '@shared/services/snackbar/snackbar.service';
import { ConsultaService } from '@shared/services/consulta/consulta.service';
import { PosMontagemService } from '@shared/services/pos-montagem/pos-montagem.service';
import { DialogService } from '@shared/services/dialog/dialog.service';
import { idDialogFormulario } from '../../constants';
import { ConsolidateAndAnlytical } from '@shared/interfaces/ConsolidatedAndAnalyticalExtract';
import { IConfirmationDialog } from '@shared/interfaces/ConfirmationDialog';
import { ISuccessDialog } from '@shared/interfaces/SuccessDialog';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {

  firstForm: FormGroup;
  secondForm: FormGroup;
  modalities: any[];
  modalitiesFilteredBySelectedProduct: any[];
  coins: any[];
  indexers: any[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      oldCodigoModalidade: number,
      oldCodigoCliente: number,
      oldCodigoEmpresa: number,
      oldCodigoOperacaoOrigem: number,
      mode: string,
      data: ConsolidateAndAnlytical
    },
    private formBuilder: FormBuilder,
    private consultaService: ConsultaService,
    private posMontagemService: PosMontagemService,
    private snackbar: SnackbarService,
    private dialog: DialogService,
    private matDialog: MatDialog
  ) {
    this.modalities = [];
    this.modalitiesFilteredBySelectedProduct = [];
    this.coins = [];
    this.indexers = [];
    this.createReactForms();
  }

  ngOnInit(): void {
    this.getModalities();
    this.getCurrency();
    this.getIndexers();
    this.getReferenceDate();
  }

  createReactForms(): void {
    this.firstForm = this.formBuilder.group({
      codigoEmpresa: this.isModeEdit ? new FormControl({ value: '', disabled: true }) : ['', Validators.required],
      codigoCliente: this.isModeEdit ? new FormControl({ value: '', disabled: true }) : ['', Validators.required],
      codigoModalidade: ['', Validators.required],
      codigoOperacaoOrigem: this.isModeEdit ? new FormControl({ value: '', disabled: true }) : ['', Validators.required],
      codigoContrato: this.isModeEdit ? new FormControl({ value: '', disabled: true }) : [''],
      nomeCliente: [''],
      endereco1: [''],
      endereco2: [''],
      endereco3: [''],
      nomeAosCuidados: this.isModeEdit ? new FormControl({ value: '', disabled: true }) : [''],
      descricaoObjetoNegociacao: [''],
      nomeEmpresa: [''],
      cnpjCpf: [''],
      codigoGrupoProduto: [''],
      codigoFamiliaProduto: [''],
      codigoProduto: [''],
      codigoMoeda: [''],
      nomeIndexador: [''],
    });

    this.secondForm = this.formBuilder.group({
      siglaSistema: [''],
      siglaModulo: [''],
      quantidade: [''],
      valorContabil: [''],
      valorCorrigidoMoedaEstrangeira: [''],
      valorOperacaoMoedaEstrangeira: [''],
      valorOperacaoMoedaNacional: [''],
      valorOperacaoMoedaEmpresa: [''],
      valorJurosMoedaEstrangeira: this.isModeEdit ? new FormControl({ value: '', disabled: true }) : [''],
      valorJurosMoedaNacional: this.isModeEdit ? new FormControl({ value: '', disabled: true }) : [''],
      tipoContabil: ['', Validators.required],
      dataReferencia: [new Date(), Validators.required],
      dataInicioOperacao: this.isModeEdit ? new FormControl({ value: new Date(), disabled: true }) : [new Date()],
      dataVencimentoOperacao: this.isModeEdit ? new FormControl({ value: new Date(), disabled: true }) : [new Date()],
      dataLiquidacaoOperacao: [new Date()],
      codigoPais: [''],
      codigoFilial: this.isModeEdit ? new FormControl({ value: '', disabled: true }) : [''],
      codigoGarantia: this.isModeEdit ? new FormControl({ value: '', disabled: true }) : ['']
    });

    this.firstForm.get('codigoProduto')?.valueChanges
      .subscribe((id: number) => {
        if (this.modalities.length > 0) {
          this.modalitiesFilteredBySelectedProduct = this.modalities.filter(modality => modality.cdProduto === id);
          this.firstForm.get('codigoModalidade')?.setValue('');

          if (this.isModeEdit) {
            this.firstForm.get('codigoModalidade')?.setValue(this.data.data.cdModalidade);
          }
        }
      });

    if (this.isModeEdit) { this.patchValuesInForm(); }
  }

  getModalities(): void {
    this.consultaService.getModalities()
      .pipe(take(1))
      .subscribe(response => {
        this.modalities = response;

        if (this.isModeEdit) {
          this.firstForm.get('codigoProduto')?.setValue(this.data.data.cdProduto);
        }
      });
  }

  getCurrency(): void {
    this.posMontagemService.getCurrency()
      .pipe(take(1))
      .subscribe(response => {
        this.coins = response;

        if (this.isModeEdit) { this.firstForm.get('codigoMoeda')?.setValue(this.data.data.cdMoeda); }
      });
  }

  getIndexers(): void {
    this.posMontagemService.getIndexers()
      .pipe(take(1))
      .subscribe(response => {
        this.indexers = response;

        if (this.isModeEdit) { this.firstForm.get('nomeIndexador')?.setValue(this.data.data.nmIndexador); }
      });
  }

  getReferenceDate(): void {
    this.consultaService.getReferenceDate()
      .pipe(take(1))
      .subscribe(response => this.secondForm.get('dataReferencia')?.setValue(dateENUSToDateObject(response.date)));
  }

  onSubmit(): void {
    if (this.data.mode === 'new') {
      this.createMaintanance();
    } else {
      this.updateMaintenance();
    }
  }

  createMaintanance(): void {
    if (this.secondForm.valid) {
      this.dialog.openConfirmationDialog(this.getBodyConfirmDialog())
        .pipe(
          concatMap(confirm => confirm &&
            this.posMontagemService.saveMaintenance({ ...this.firstForm.value, ...this.secondForm.value }).pipe(take(1))))
        .subscribe((response: any) => {
          this.dialog.openSuccessDialog(this.getBodySuccessDialog(response, 'Registro salvo com sucesso!'));
          this.matDialog.getDialogById(idDialogFormulario)?.close();
        }, err => this.snackbar.showSnackbarError(err.message));
    }
  }


  updateMaintenance(): void {
    if (this.secondForm.valid) {
      const { oldCodigoModalidade, oldCodigoCliente, oldCodigoEmpresa, oldCodigoOperacaoOrigem } = this.data;

      this.dialog.openConfirmationDialog(this.getBodyConfirmDialog())
        .pipe(
          concatMap(confirm => confirm &&
            this.posMontagemService.updateMaintenance({
              oldCodigoModalidade,
              oldCodigoCliente,
              oldCodigoEmpresa,
              oldCodigoOperacaoOrigem,
              ...this.firstForm.value,
              ...this.secondForm.value
            }).pipe(take(1))))
        .subscribe((response: any) => {
          if (response) {
            this.dialog.openSuccessDialog(this.getBodySuccessDialog(response, 'Registro alterado com sucesso!'));
            this.matDialog.getDialogById(idDialogFormulario)?.close();
          }
        }, err => this.snackbar.showSnackbarError(err.message));
    }
  }

  closeDialog(): void {
    this.dialog.openConfirmationDialog({
      titulo: 'Alterações não foram salvas. Desconsiderar as alterações ?',
      acao: 'Confirmar'
    })
      .subscribe(confirm => confirm && this.matDialog.getDialogById(idDialogFormulario)?.close());
  }

  patchValuesInForm(): void {
    const values = this.data.data;

    this.firstForm.patchValue({
      codigoEmpresa: values.cdEmpresa,
      codigoCliente: values.cdCliente,
      codigoModalidade: values.cdModalidade,
      codigoOperacaoOrigem: values.cdOperOrigem,
      codigoContrato: values.cdContrato,
      nomeCliente: values.nmCliente,
      endereco1: values.dcEnd1,
      endereco2: values.dcEnd2,
      endereco3: values.dcEnd3,
      nomeAosCuidados: values.nomeAosCuidados,
      descricaoObjetoNegociacao: values.dsObn,
      nomeEmpresa: values.nmEmpresa,
      cnpjCpf: values.cdCgcCpf,
      codigoGrupoProduto: values.cdGrprod,
      codigoFamiliaProduto: values.cdFamprod,
      codigoProduto: values.cdProduto,
      codigoMoeda: values.cdMoeda,
      nomeIndexador: values.nmIndexador,
    });

    this.secondForm.patchValue({
      siglaSistema: values.sgSistema,
      siglaModulo: values.sgModulo,
      quantidade: values.qtde,
      valorContabil: values.vlContabil,
      valorCorrigidoMoedaEstrangeira: values.vlCorrigidoMe,
      valorOperacaoMoedaEstrangeira: values.vlOperacaoMe,
      valorOperacaoMoedaNacional: values.vlOperacaoMn,
      valorOperacaoMoedaEmpresa: values.vlOperacaoMe,
      valorJurosMoedaEstrangeira: values.vlJurosMe,
      valorJurosMoedaNacional: values.vlJurosMn,
      tipoContabil: values.iiTipoContabil,
      dataReferencia: values.dtReferencia ? dateENUSToDateObject(values.dtReferencia) : null,
      dataInicioOperacao: values.dtInicioOper ? dateENUSToDateObject(values.dtInicioOper) : null,
      dataVencimentoOperacao: values.dtVencimentoOper ? dateENUSToDateObject(values.dtVencimentoOper) : null,
      dataLiquidacaoOperacao: values.dtLiquidacaoOper ? dateENUSToDateObject(values.dtLiquidacaoOper) : null,
      codigoPais: values.cdPais,
      codigoFilial: values.cdFilial,
      codigoGarantia: values.cdGarantia
    });
  }

  getBodyConfirmDialog(): IConfirmationDialog {
    const { oldCodigoCliente, oldCodigoOperacaoOrigem } = this.data;
    const {
      codigoCliente = oldCodigoCliente,
      codigoOperacaoOrigem = oldCodigoOperacaoOrigem,
      codigoModalidade
    } = this.firstForm.value;

    return {
      titulo: 'Salvar registro:',
      acao: 'Salvar',
      dados: [
        { name: 'Código de Operação de Origem', data: codigoOperacaoOrigem },
        { name: 'Código Cliente', data: codigoCliente },
        { name: 'Código Modalidade', data: codigoModalidade }
      ],
      format: {
        key: 'name',
        value: 'data',
      },
    };
  }

  getBodySuccessDialog(response: any, text: string): ISuccessDialog {
    const { cdEmpresa, cdCliente, cdModalidade } = response;

    return {
      titulo: `${text} As alterações realizadas têm de ser aprovadas por outro usuário para que elas tenham efeito.`,
      dados: [
        { key: 'Código Empresa', value: cdEmpresa },
        { key: 'Código Cliente', value: cdCliente },
        { key: 'Código Modalidade', value: cdModalidade }
      ],
      format: {
        key: 'key',
        value: 'value',
      },
    };
  }

  get isModeEdit(): boolean {

    return this.data.mode === 'edit';
  }

  get hasProductSelected(): boolean {

    const value = this.firstForm.get('codigoProduto')?.value;

    return value !== '' && value !== null;
  }
}
