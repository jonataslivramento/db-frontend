import { Component, ChangeDetectionStrategy, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Company } from '@shared/interfaces/Company';
import { ConsultaService } from '@shared/services/consulta/consulta.service';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { initialStateValuesForSubmit } from '../../constants'

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchFormComponent implements OnInit {
  searchForm: FormGroup;
  companies: Company[];
  modalities$: Observable<any>;
  lastDate: string;
  @Output() handleSubmit: EventEmitter<any>;
  @Output() handleReset: EventEmitter<any>;

  constructor(private formBuilder: FormBuilder, private consultaService: ConsultaService) {
    this.handleSubmit = new EventEmitter();
    this.handleReset = new EventEmitter();
    this.searchForm = this.formBuilder.group(initialStateValuesForSubmit);
  }

  ngOnInit(): void {
    this.getCompanies();
    this.getModalities();
    this.getReferenceDate();
  }

  onSubmit(): void {
    this.handleSubmit.emit(this.searchForm.value);
  }

  resetFilter(): void {
    this.searchForm.reset();
    this.searchForm.get('modalidade')?.setValue('all');
    this.searchForm.get('codigoEmpresa')?.setValue(101101);
    this.handleReset.emit();
  }

  getReferenceDate(): void {
    this.consultaService.getReferenceDate()
      .pipe(take(1))
      .subscribe(response => {
        this.lastDate = response.date;
      });
  }

  getCompanies(): void {
    this.consultaService.getCompanies()
      .pipe(take(1))
      .subscribe(response => {
        this.companies = response;
        this.searchForm.get('codigoEmpresa')?.setValue(101101);
      });
  }

  getModalities(): void {
    this.modalities$ = this.consultaService.getModalities();
  }

  get hasFieldFilled(): boolean {
    return Boolean(this.searchForm.get('modalidade')?.value ||
      this.searchForm.get('codigoCliente')?.value ||
      this.searchForm.get('codigoEmpresa')?.value ||
      this.searchForm.get('codigoOperacao')?.value);
  }

}
