import { MatPaginator } from '@angular/material/paginator';
import { Component, Input, Output, EventEmitter, ViewChild, OnChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { colunasPosMontagem, itensPorPagina } from '@shared/constants';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnChanges {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Input() dataSource: MatTableDataSource<any>;
  @Input() totalElements: number;
  @Input() pageSize: number;
  @Input() selection: SelectionModel<any>;
  @Input() loadingClients: boolean;
  @Output() handleChangePage: EventEmitter<any>;
  @Output() changeMatSort: EventEmitter<MatSort>;
  displayedColumns: string[];
  pageSizeOptions: number[];

  constructor() {
    this.pageSizeOptions = itensPorPagina;
    this.displayedColumns = colunasPosMontagem;
    this.selection = new SelectionModel<any>(false, []);
    this.handleChangePage = new EventEmitter();
    this.changeMatSort = new EventEmitter<MatSort>()
  }

  ngOnChanges(): void {
    this.changeMatSort.emit(this.sort);
  }

  changePage(value: any): void {
    this.handleChangePage.emit(value);
  }
}
