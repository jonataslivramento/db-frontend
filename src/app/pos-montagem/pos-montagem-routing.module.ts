import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PosMontagemComponent } from './pos-montagem.component';

const routes: Routes = [{ path: '', component: PosMontagemComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PosMontagemRoutingModule { }
