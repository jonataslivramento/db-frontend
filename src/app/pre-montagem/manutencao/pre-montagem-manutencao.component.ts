import { PreMontagemManutencaoListComponent } from './pre-montagem-manutencao-list/pre-montagem-manutencao-list.component';
import { PreMontagemParametrizacaoListComponent } from './../parametrizacao/pre-montagem-parametrizacao-list/pre-montagem-parametrizacao-list.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { DestroyService } from '@shared/services/destroy/destroy.service';
import { PreMontagemService } from '@shared/services/pre-montagem/pre-montagem.service';
import { Observable, of } from 'rxjs';
import { catchError, finalize, takeUntil } from 'rxjs/operators';
import { removeEmpty } from './../../shared/constants/index';

@Component({
  selector: 'app-pre-montagem-manutencao',
  templateUrl: './pre-montagem-manutencao.component.html',
  styleUrls: ['./pre-montagem-manutencao.component.scss'],
  providers: [DestroyService],
})
export class PreMontagemManutencaoComponent implements OnInit {
  @ViewChild('preMontagemManutencaoList')
  preMontagemManutencaoList: PreMontagemManutencaoListComponent;
  loadPreMontagem = false;
  loaded = false;
  preMontagemResponse = null;
  parameters: any = {
    cdApplication: '',
    cdModalidade: '',
    cdSeqModAplic: '',
  };
  pageSize = 5;
  pageIndex = 0;

  constructor(
    private preMontagemService: PreMontagemService,
    private destroy$: DestroyService
  ) {}

  ngOnInit(): void {}

  async getPreMontagemList(): Promise<void> {
    let parameters: any = {
      page: this.pageIndex.toLocaleString().replace(/,/g, ''),
      size: this.pageSize.toLocaleString(),
      cdApplication: this.parameters?.cdApplication?.toString(),
      cdModalidade: this.parameters?.cdModalidade?.toString(),
      cdSeqModAplic: this.parameters?.cdSeqModAplic?.toString(),
    };

    parameters = await removeEmpty(parameters);

    this.loadPreMontagem = true;
    this.preMontagemService
      .getPreMontagemList(parameters)
      .pipe(
        takeUntil(this.destroy$),
        catchError((e) => this.handleError(e)),
        finalize(() => {
          this.loadPreMontagem = false;
          this.loaded = true;
        })
      )
      .subscribe((response: any) => {
        this.preMontagemResponse = response;
      });
  }

  handleSearch(parameters: any): void {
    this.parameters = parameters;
    this.pageIndex = 0;
    this.preMontagemManutencaoList.paginator.firstPage();
    this.getPreMontagemList();
  }

  handleChangePage(pageEvent: PageEvent): void {
    this.pageIndex = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;
    this.getPreMontagemList();
  }

  handleClearMontagem(parameters: any): void {
    this.parameters = parameters;
    this.preMontagemManutencaoList.recarregar();
  }

  handleError(error: any): Observable<any> {
    this.preMontagemResponse = null;
    return of();
  }

  handleRecarregar(): void {
    this.pageIndex = 0;
    this.getPreMontagemList();
  }
}
