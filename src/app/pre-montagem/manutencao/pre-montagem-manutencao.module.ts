import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { PreMontagemManutencaoFormularioDialogComponent } from './pre-montagem-manutencao-formulario/pre-montagem-manutencao-formulario.component';
import { PreMontagemManutencaoListComponent } from './pre-montagem-manutencao-list/pre-montagem-manutencao-list.component';
import { PreMontagemManutencaoRoutingModule } from './pre-montagem-manutencao-routing.module';
import { PreMontagemManutencaoSearchComponent } from './pre-montagem-manutencao-search/pre-montagem-manutencao-search.component';
import { PreMontagemManutencaoComponent } from './pre-montagem-manutencao.component';

@NgModule({
  declarations: [
    PreMontagemManutencaoComponent,
    PreMontagemManutencaoSearchComponent,
    PreMontagemManutencaoListComponent,
    PreMontagemManutencaoFormularioDialogComponent,
  ],
  imports: [
    CommonModule,
    PreMontagemManutencaoRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class PreMontagemManutencaoModule {}
