import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreMontagemManutencaoSearchComponent } from './pre-montagem-manutencao-search.component';

describe('PreMontagemManutencaoSearchComponent', () => {
  let component: PreMontagemManutencaoSearchComponent;
  let fixture: ComponentFixture<PreMontagemManutencaoSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreMontagemManutencaoSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreMontagemManutencaoSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
