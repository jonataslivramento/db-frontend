import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Modality } from '@shared/interfaces/Modality';
import { ConsultaService } from '@shared/services/consulta/consulta.service';
import { Observable } from 'rxjs';
import {
  DESTINO_EXTRATO,
  ORIGEM_MIDAS,
} from '@shared/constants/pre-montagem';

@Component({
  selector: 'app-pre-montagem-manutencao-search',
  templateUrl: './pre-montagem-manutencao-search.component.html',
  styleUrls: ['./pre-montagem-manutencao-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreMontagemManutencaoSearchComponent implements OnInit {
  @Output() searchMontagemEmitter: EventEmitter<any> = new EventEmitter();
  @Output() clearMontagemEmitter: EventEmitter<any> = new EventEmitter();
  modalities$: Observable<Modality[]>;
  manutencaoSearchForm: FormGroup;
  origemMidas = ORIGEM_MIDAS;
  destinoExtratos = DESTINO_EXTRATO;

  constructor(
    private formBuilder: FormBuilder,
    private consultaService: ConsultaService
  ) {
    this.manutencaoSearchForm = this.formBuilder.group({
      cdApplication: '',
      cdModalidade: '',
      cdSeqModAplic: '',
    });
  }

  ngOnInit(): void {
    this.getModalities();
  }

  getModalities(): void {
    this.modalities$ = this.consultaService.getModalities();
  }

  onSubmit(): void {
    this.searchMontagemEmitter.emit(this.manutencaoSearchForm.value);
  }

  resetFilter(): void {
    this.manutencaoSearchForm.reset();
    this.manutencaoSearchForm.get('cdApplication')?.setValue('');
    this.manutencaoSearchForm.get('cdModalidade')?.setValue('');
    this.manutencaoSearchForm.get('cdSeqModAplic')?.setValue('');
    this.clearMontagemEmitter.emit(this.manutencaoSearchForm.value);
  }

  get montagemSearchFormFilled(): boolean {
    return Boolean(
      this.manutencaoSearchForm.get('cdApplication')?.value ||
        this.manutencaoSearchForm.get('cdModalidade')?.value ||
        this.manutencaoSearchForm.get('cdSeqModAplic')?.value
    );
  }
}
