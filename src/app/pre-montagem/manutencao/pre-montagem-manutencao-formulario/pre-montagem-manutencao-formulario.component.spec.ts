import { PreMontagemManutencaoFormularioDialogComponent } from './pre-montagem-manutencao-formulario.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';



describe('PreMontagemManutencaoFormularioDialogComponent', () => {
  let component: PreMontagemManutencaoFormularioDialogComponent;
  let fixture: ComponentFixture<PreMontagemManutencaoFormularioDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreMontagemManutencaoFormularioDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreMontagemManutencaoFormularioDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
