import { DialogService } from './../../../shared/services/dialog/dialog.service';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Indexer } from '@shared/interfaces/Indexer';
import { Modality } from '@shared/interfaces/Modality';
import { ConsultaService } from '@shared/services/consulta/consulta.service';
import { PreMontagemService } from '@shared/services/pre-montagem/pre-montagem.service';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { IPreMontagemManutencao } from './../../../shared/interfaces/PreMontagemManutencao';
import { SourceSystem } from './../../../shared/interfaces/SourceSystem';

@Component({
  templateUrl: './pre-montagem-manutencao-formulario.component.html',
  styleUrls: ['./pre-montagem-manutencao-formulario.component.scss'],
})
export class PreMontagemManutencaoFormularioDialogComponent implements OnInit {
  preMontagemFormGroup: FormGroup;
  modalities$: Observable<Modality[]>;
  indexers$: Observable<Indexer[]>;
  sourceSystem$: Observable<SourceSystem[]>;

  constructor(
    private formBuilder: FormBuilder,
    private consultaService: ConsultaService,
    private dialogService: DialogService,
    private preMontagemService: PreMontagemService,
    public dialogRef: MatDialogRef<PreMontagemManutencaoFormularioDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IPreMontagemManutencao
  ) {
    this.preMontagemFormGroup = this.formBuilder.group({
      cdApplication: ['1', Validators.required],
      cdModalidade: ['', Validators.required],
      cdGarantia: null,
      cdSeqModAplic: [{ value: null, disabled: true }],
      cdSisOrig: ['', Validators.required],
      cgModulo: [null, Validators.maxLength(2)],
      cgSistema: [null, Validators.maxLength(2)],
      displayMe: null,
      displayMn: null,
      displayQtde: null,
      dsNegocio: null,
      dsNegocioIngles: null,
      dsObn: null,
      iiTipoContabil: ['A', Validators.required],
      modalidade: '',
      nmIndexador: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.getModalities();
    this.getSistemaOrigem();
    this.getIndexesMidas();
    if (this.data) {
      this.updateForm();
    }
  }

  updateForm(): void {
    this.preMontagemFormGroup.patchValue({
      cdApplication: this.data.cdApplication.toString(),
      cdModalidade: this.data.cdModalidade,
      cdGarantia: this.data.cdGarantia,
      cdSeqModAplic: this.data.cdSeqModAplic,
      cdSisOrig: this.data.cdSisOrig,
      cgModulo: this.data.cgModulo,
      cgSistema: this.data.cgSistema.trim(),
      displayMe: this.data.displayMe,
      displayMn: this.data.displayMn,
      displayQtde: this.data.displayQtde,
      dsNegocio: this.data.dsNegocio,
      dsNegocioIngles: this.data.dsNegocioIngles,
      dsObn: this.data.dsObn,
      iiTipoContabil: this.data.iiTipoContabil,
      modalidade: this.data.modalidade,
      nmIndexador: this.data.nmIndexador,
    });
  }

  getModalities(): void {
    this.modalities$ = this.consultaService.getModalities();
  }

  getSistemaOrigem(): void {
    this.sourceSystem$ = this.consultaService.getSourceSystem();
  }

  getIndexesMidas(): void {
    this.indexers$ = this.consultaService.getIndexers();
  }

  salvarModalidade(): void {
    if (this.preMontagemFormGroup.invalid) {
      this.preMontagemFormGroup.markAllAsTouched();
      return;
    }
    const displayMeControl = this.preMontagemFormGroup.get('displayMe')?.value;
    const displayMnControl = this.preMontagemFormGroup.get('displayMn')?.value;
    const displayQtdeControl = this.preMontagemFormGroup.get('displayQtde')
      ?.value;
    this.preMontagemFormGroup.patchValue({
      displayMe: displayMeControl ? 'S' : null,
      displayMn: displayMnControl ? 'S' : null,
      displayQtde: displayQtdeControl ? 'S' : null,
    });

    if (this.data?.cdSeqModAplic) {
      this.updateModalidade();
    } else {
      this.addModalidade();
    }
  }

  addModalidade(): void {
    this.preMontagemService
      .addModalidade(this.preMontagemFormGroup.value)
      .pipe(take(1))
      .subscribe((response) => {
        if(response) {
          this.dialogService.openSuccessDialog({
            titulo: 'Dados inseridos com sucesso',
            dados: [
              {
                item: 'Cód. Sequencial',
                cdSeqModAplic: response.cdSeqModAplic,
              },
            ],
            format: {
              key: 'item',
              value: 'cdSeqModAplic',
            },
          });
        } else {
          this.dialogService.openSuccessDialog({
            titulo: 'Dados inseridos com sucesso',
          });
        }
        this.dialogRef.close(true);
      });
  }

  updateModalidade(): void {
    const dialog = this.dialogService.openConfirmationDialog({
      titulo: 'Salvar alterações para',
      acao: 'Salvar',
      dados: [
        {
          item: 'Cód. Sequencial',
          cdSeqModAplic: this.preMontagemFormGroup.getRawValue().cdSeqModAplic,
        },
      ],
      format: {
        key: 'item',
        value: 'cdSeqModAplic',
      },
    });

    dialog.pipe(take(1)).subscribe((response) => {
      if (response) {
        this.preMontagemService
          .updateModalidade(this.preMontagemFormGroup.getRawValue())
          .pipe(take(1))
          .subscribe(() => {
            this.dialogRef.close(true);
            this.dialogService.openSuccessDialog({
              titulo: 'Alterações salvas para',
              dados: [
                {
                  item: 'Cód. Sequencial',
                  cdSeqModAplic: this.preMontagemFormGroup.getRawValue()
                    .cdSeqModAplic,
                },
              ],
              format: {
                key: 'item',
                value: 'cdSeqModAplic',
              },
            });
          });
      }
    });
  }

  close(): void {
    const dialog = this.dialogService.openConfirmationDialog({
      titulo: 'Formulário de manutenção',
      acao: 'Confirmar e sair',
      dados: [
        {
          item: `O processo de inserir uma nova Manutenção de Modalidade não foi concluído,
           caso confirme a saída os dados não serão salvos`,
          message: '',
        },
      ],
      format: {
        key: 'item',
        value: 'message',
        separator: '',
      },
    });

    dialog.pipe(take(1)).subscribe((response) => {
      if (response) {
        this.dialogRef.close();
      }
    });
  }
}
