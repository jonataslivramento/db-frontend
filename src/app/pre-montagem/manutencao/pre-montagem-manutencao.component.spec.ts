import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreMontagemManutencaoComponent } from './pre-montagem-manutencao.component';

describe('PreMontagemManutencaoComponent', () => {
  let component: PreMontagemManutencaoComponent;
  let fixture: ComponentFixture<PreMontagemManutencaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreMontagemManutencaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreMontagemManutencaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
