import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreMontagemManutencaoListComponent } from './pre-montagem-manutencao-list.component';

describe('PreMontagemManutencaoListComponent', () => {
  let component: PreMontagemManutencaoListComponent;
  let fixture: ComponentFixture<PreMontagemManutencaoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreMontagemManutencaoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreMontagemManutencaoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
