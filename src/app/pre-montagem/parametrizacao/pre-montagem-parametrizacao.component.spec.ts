import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreMontagemParametrizacaoComponent } from './pre-montagem-parametrizacao.component';

describe('PreMontagemParametrizacaoComponent', () => {
  let component: PreMontagemParametrizacaoComponent;
  let fixture: ComponentFixture<PreMontagemParametrizacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreMontagemParametrizacaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreMontagemParametrizacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
