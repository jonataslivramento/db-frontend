import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreMontagemParametrizacaoComponent } from './pre-montagem-parametrizacao.component';

const routes: Routes = [
  { path: '', component: PreMontagemParametrizacaoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreMontagemParametrizacaoRoutingModule {}
