import { SourceSystem } from './../../../shared/interfaces/SourceSystem';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Modality } from '@shared/interfaces/Modality';
import { ConsultaService } from '@shared/services/consulta/consulta.service';
import { Observable } from 'rxjs';
import { DESTINO_EXTRATO, ORIGEM_MIDAS } from '@shared/constants/pre-montagem';

@Component({
  selector: 'app-pre-montagem-parametrizacao-search',
  templateUrl: './pre-montagem-parametrizacao-search.component.html',
  styleUrls: ['./pre-montagem-parametrizacao-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreMontagemParametrizacaoSearchComponent implements OnInit {
  @Output() searchMontagemEmitter: EventEmitter<any> = new EventEmitter();
  @Output() clearParametrizacaoEmitter: EventEmitter<any> = new EventEmitter();
  modalities$: Observable<Modality[]>;
  sourceSystem$: Observable<SourceSystem[]>;
  parametrizacaoSearchForm: FormGroup;
  origemMidas = ORIGEM_MIDAS;
  destinoExtratos = DESTINO_EXTRATO;

  constructor(
    private formBuilder: FormBuilder,
    private consultaService: ConsultaService
  ) {
    this.parametrizacaoSearchForm = this.formBuilder.group({
      cdModalidade: '',
      cdSisOrig: '',
      iddDCamd: '',
      iddDCaex: '',
    });
  }

  ngOnInit(): void {
    this.getModalities();
    this.getSourceSystem();
  }

  getModalities(): void {
    this.modalities$ = this.consultaService.getModalities();
  }

  getSourceSystem(): void {
    this.sourceSystem$ = this.consultaService.getSourceSystem();
  }

  onSubmit(): void {
    this.searchMontagemEmitter.emit(this.parametrizacaoSearchForm.value);
  }

  resetFilter(): void {
    this.parametrizacaoSearchForm.reset();
    this.parametrizacaoSearchForm.get('cdModalidade')?.setValue('');
    this.parametrizacaoSearchForm.get('cdSisOrig')?.setValue('');
    this.parametrizacaoSearchForm.get('iddDCamd')?.setValue('');
    this.parametrizacaoSearchForm.get('iddDCaex')?.setValue('');
    this.clearParametrizacaoEmitter.emit(this.parametrizacaoSearchForm.value);
  }

  get parametrizacaoSearchFormFilled(): boolean {
    return Boolean(
      this.parametrizacaoSearchForm.get('cdModalidade')?.value ||
        this.parametrizacaoSearchForm.get('cdSisOrig')?.value ||
        this.parametrizacaoSearchForm.get('iddDCamd')?.value ||
        this.parametrizacaoSearchForm.get('iddDCaex')?.value
    );
  }
}
