import { PreMontagemParametrizacaoSearchComponent } from './pre-montagem-parametrizacao-search.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';



describe('PreMontagemParametrizacaoSearchComponent', () => {
  let component: PreMontagemParametrizacaoSearchComponent;
  let fixture: ComponentFixture<PreMontagemParametrizacaoSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreMontagemParametrizacaoSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreMontagemParametrizacaoSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
