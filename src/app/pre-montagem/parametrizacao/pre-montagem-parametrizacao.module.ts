import { PreMontagemParametrizacaoFormularioDialogComponent } from './pre-montagem-parametrizacao-formulario/pre-montagem-parametrizacao-formulario.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { PreMontagemParametrizacaoListComponent } from './pre-montagem-parametrizacao-list/pre-montagem-parametrizacao-list.component';
import { PreMontagemParametrizacaoRoutingModule } from './pre-montagem-parametrizacao-routing.module';
import { PreMontagemParametrizacaoSearchComponent } from './pre-montagem-parametrizacao-search/pre-montagem-parametrizacao-search.component';
import { PreMontagemParametrizacaoComponent } from './pre-montagem-parametrizacao.component';

@NgModule({
  declarations: [
    PreMontagemParametrizacaoComponent,
    PreMontagemParametrizacaoSearchComponent,
    PreMontagemParametrizacaoListComponent,
    PreMontagemParametrizacaoFormularioDialogComponent,
  ],
  imports: [
    CommonModule,
    PreMontagemParametrizacaoRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class PreMontagemParametrizacaoModule {}
