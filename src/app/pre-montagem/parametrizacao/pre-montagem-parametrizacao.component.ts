import { PreMontagemParametrizacaoListComponent } from './pre-montagem-parametrizacao-list/pre-montagem-parametrizacao-list.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { DestroyService } from '@shared/services/destroy/destroy.service';
import { PreMontagemService } from '@shared/services/pre-montagem/pre-montagem.service';
import { Observable, of } from 'rxjs';
import { catchError, finalize, takeUntil } from 'rxjs/operators';
import { removeEmpty } from './../../shared/constants/index';

@Component({
  selector: 'app-pre-montagem-parametrizacao',
  templateUrl: './pre-montagem-parametrizacao.component.html',
  styleUrls: ['./pre-montagem-parametrizacao.component.scss'],
  providers: [DestroyService],
})
export class PreMontagemParametrizacaoComponent implements OnInit {
  @ViewChild('preMontagemParametrizacaoList')
  preMontagemParametrizacaoList: PreMontagemParametrizacaoListComponent;
  loadPreMontagem = false;
  loaded = false;
  preMontagemResponse = null;
  parameters: any = {
    cdModalidade: '',
    cdSisOrig: '',
    iddDCamd: '',
    iddDCaex: '',
  };
  pageSize = 5;
  pageIndex = 0;

  constructor(
    private preMontagemService: PreMontagemService,
    private destroy$: DestroyService
  ) {}

  ngOnInit(): void {}

  async getPreMontagemList(): Promise<void> {
    let parameters: any = {
      page: this.pageIndex.toLocaleString().replace(/,/g, ''),
      size: this.pageSize.toLocaleString(),
      cdModalidade: this.parameters?.cdModalidade?.toString(),
      cdSisOrig: this.parameters?.cdSisOrig?.toString(),
      iddDCamd: this.parameters?.iddDCamd?.toString(),
      iddDCaex: this.parameters?.iddDCaex?.toString(),
    };

    parameters = await removeEmpty(parameters);

    this.loadPreMontagem = true;
    this.preMontagemService
      .getPreMontagemParametrizacaoList(parameters)
      .pipe(
        takeUntil(this.destroy$),
        catchError((e) => this.handleError(e)),
        finalize(() => {
          this.loadPreMontagem = false;
          this.loaded = true;
        })
      )
      .subscribe((response: any) => {
        this.preMontagemResponse = response;
      });
  }

  handleSearch(parameters: any): void {
    this.parameters = parameters;
    this.pageIndex = 0;
    this.preMontagemParametrizacaoList.paginator.firstPage();
    this.getPreMontagemList();
  }

  handleChangePage(pageEvent: PageEvent): void {
    this.pageIndex = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;
    this.getPreMontagemList();
  }

  handleClearParametrizacao(parameters: any): void {
    this.parameters = parameters;
    this.preMontagemParametrizacaoList.recarregar();
  }

  handleError(error: any): Observable<any> {
    this.preMontagemResponse = null;
    return of();
  }

  handleRecarregar(): void {
    this.pageIndex = 0;
    this.getPreMontagemList();
  }
}
