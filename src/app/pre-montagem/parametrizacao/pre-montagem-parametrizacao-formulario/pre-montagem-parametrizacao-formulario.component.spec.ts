import { PreMontagemParametrizacaoFormularioDialogComponent } from './pre-montagem-parametrizacao-formulario.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';



describe('PreMontagemParametrizacaoFormularioDialogComponent', () => {
  let component: PreMontagemParametrizacaoFormularioDialogComponent;
  let fixture: ComponentFixture<PreMontagemParametrizacaoFormularioDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreMontagemParametrizacaoFormularioDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreMontagemParametrizacaoFormularioDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
