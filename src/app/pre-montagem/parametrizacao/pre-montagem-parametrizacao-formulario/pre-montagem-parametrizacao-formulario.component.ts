import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DESTINO_EXTRATO, ORIGEM_MIDAS } from '@shared/constants/pre-montagem';
import { Currency } from '@shared/interfaces/Currency';
import { Indexer } from '@shared/interfaces/Indexer';
import { Modality } from '@shared/interfaces/Modality';
import { Product } from '@shared/interfaces/Product';
import { SourceSystem } from '@shared/interfaces/SourceSystem';
import { ConsultaService } from '@shared/services/consulta/consulta.service';
import { PreMontagemService } from '@shared/services/pre-montagem/pre-montagem.service';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { IPreMontagemParametrizacao } from './../../../shared/interfaces/PreMontagem';
import { DialogService } from './../../../shared/services/dialog/dialog.service';

@Component({
  templateUrl: './pre-montagem-parametrizacao-formulario.component.html',
  styleUrls: ['./pre-montagem-parametrizacao-formulario.component.scss'],
})
export class PreMontagemParametrizacaoFormularioDialogComponent
  implements OnInit {
  origemMidas = ORIGEM_MIDAS;
  destinoExtratos = DESTINO_EXTRATO;
  firstTimeEdit = true;
  preMontagemFormGroup: FormGroup;
  modalities: Modality[] = [];
  modalitiesFilteredBySelectedProduct: Modality[] = [];
  indexers$: Observable<Indexer[]>;
  sourceSystem$: Observable<SourceSystem[]>;
  products$: Observable<Product[]>;
  currencies$: Observable<Currency[]>;

  constructor(
    private formBuilder: FormBuilder,
    private consultaService: ConsultaService,
    private dialogService: DialogService,
    private preMontagemService: PreMontagemService,
    public dialogRef: MatDialogRef<PreMontagemParametrizacaoFormularioDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IPreMontagemParametrizacao
  ) {
    this.preMontagemFormGroup = this.formBuilder.group({
      cdSeqModalidade: [{ value: null, disabled: true }],
      cdModalidade: [{ value: '', disabled: true }, Validators.required],
      cdSisOrig: ['', Validators.required],
      qtCasaDec: null,
      iddDTrro: ['TRUN', Validators.required],
      iddDCamd: ['VCTB', Validators.required],
      iddDCaex: ['QTDE', Validators.required],
      iddDSnop: ['ABSL', Validators.required],
      cdProduto: ['', Validators.required],
      cdMoedaOper: ['', Validators.required],
    });
    this.subscribeCdProduto();
  }

  subscribeCdProduto(): void {
    const cdModalidadeControl = this.preMontagemFormGroup.get('cdModalidade');
    this.preMontagemFormGroup
      .get('cdProduto')
      ?.valueChanges.subscribe((id: number) => {
        if (this.modalities.length > 0) {
          this.modalitiesFilteredBySelectedProduct = this.modalities.filter(
            (modality) => modality.cdProduto === id
          );
          cdModalidadeControl?.setValue('');
          cdModalidadeControl?.setValidators([Validators.required]);
          cdModalidadeControl?.updateValueAndValidity();
          cdModalidadeControl?.enable();
          if (this.data && this.firstTimeEdit) {
            this.firstTimeEdit = false;
            cdModalidadeControl?.setValue(this.data.cdModalidade);
          }
        }
      });
  }

  ngOnInit(): void {
    this.getModalities();
    this.getSourceSystem();
    this.getIndexesMidas();
    this.getProducts();
    this.getCurrencies();

    if (this.data) {
      this.updateForm();
    }
  }

  updateForm(): void {
    this.preMontagemFormGroup.patchValue({
      cdSeqModalidade: this.data.cdSeqModalidade,
      cdModalidade: this.data.cdModalidade,
      cdSisOrig: this.data.cdSisOrig.trim(),
      qtCasaDec: this.data.qtCasaDec,
      iddDTrro: this.data.iddDTrro,
      iddDCamd: this.data.iddDCamd,
      iddDCaex: this.data.iddDCaex,
      iddDSnop: this.data.iddDSnop,
      cdProduto: this.data.cdProduto,
      cdMoedaOper: this.data.cdMoedaOper,
    });
  }

  getModalities(): void {
    this.consultaService
      .getModalities()
      .pipe(take(1))
      .subscribe((response) => {
        this.modalities = response;
        if (this.data) {
          this.preMontagemFormGroup
            .get('cdProduto')
            ?.setValue(this.data.cdProduto);
        }
      });
  }

  getSourceSystem(): void {
    this.sourceSystem$ = this.consultaService.getSourceSystem();
  }

  getIndexesMidas(): void {
    this.indexers$ = this.consultaService.getIndexers();
  }

  getProducts(): void {
    this.products$ = this.consultaService.getProducts();
  }

  getCurrencies(): void {
    this.currencies$ = this.consultaService.getCurrency();
  }

  salvarModalidade(): void {
    if (this.preMontagemFormGroup.invalid) {
      this.preMontagemFormGroup.markAllAsTouched();
      return;
    }
    if (this.data?.cdSeqModalidade) {
      this.updateParametrizacao();
    } else {
      this.addParametrizacao();
    }
  }

  addParametrizacao(): void {
    this.preMontagemService
      .addParametrizacao(this.preMontagemFormGroup.value)
      .pipe(take(1))
      .subscribe((response) => {
        if (response) {
          this.dialogService.openSuccessDialog({
            titulo: 'Dados inseridos com sucesso',
            dados: [
              {
                item: 'Cód. Sequencial',
                cdSeqModalidade: response.cdSeqModalidade,
              },
            ],
            format: {
              key: 'item',
              value: 'cdSeqModalidade',
            },
          });
        } else {
          this.dialogService.openSuccessDialog({
            titulo: 'Dados inseridos com sucesso',
          });
        }

        this.dialogRef.close(true);
      });
  }

  updateParametrizacao(): void {
    const dialog = this.dialogService.openConfirmationDialog({
      titulo: 'Salvar alterações para',
      acao: 'Salvar',
      dados: [
        {
          item: 'Cód. Sequencial',
          cdSeqModalidade: this.preMontagemFormGroup.getRawValue()
            .cdSeqModalidade,
        },
      ],
      format: {
        key: 'item',
        value: 'cdSeqModalidade',
      },
    });

    dialog.pipe(take(1)).subscribe((response) => {
      if (response) {
        this.preMontagemService
          .updateParametrizacao(this.preMontagemFormGroup.getRawValue())
          .pipe(take(1))
          .subscribe(() => {
            this.dialogRef.close(true);
            this.dialogService.openSuccessDialog({
              titulo: 'Alterações salvas para',
              dados: [
                {
                  item: 'Cód. Sequencial',
                  cdSeqModalidade: this.preMontagemFormGroup.getRawValue()
                    .cdSeqModalidade,
                },
              ],
              format: {
                key: 'item',
                value: 'cdSeqModalidade',
              },
            });
          });
      }
    });
  }

  close(): void {
    const dialog = this.dialogService.openConfirmationDialog({
      titulo: 'Formulário de manutenção',
      acao: 'Confirmar e sair',
      dados: [
        {
          item: `O processo de inserir uma nova Parametrização de Modalidade não foi concluído,
           caso confirme a saída os dados não serão salvos`,
          message: '',
        },
      ],
      format: {
        key: 'item',
        value: 'message',
        separator: '',
      },
    });

    dialog.pipe(take(1)).subscribe((response) => {
      if (response) {
        this.dialogRef.close();
      }
    });
  }

  get hasProductSelected(): boolean {
    const value = this.preMontagemFormGroup.get('cdProduto')?.value;

    return value !== '' && value !== null;
  }
}
