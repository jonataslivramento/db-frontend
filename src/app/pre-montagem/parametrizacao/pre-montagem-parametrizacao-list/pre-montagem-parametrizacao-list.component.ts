import { SelectionModel } from '@angular/cdk/collections';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmationDialogComponent } from '@shared/components/confirmation-dialog/confirmation-dialog.component';
import { itensPorPagina } from '@shared/constants';
import { colunasPreMontagemParametrizacao } from '@shared/constants/pre-montagem';
import { PreMontagemService } from '@shared/services/pre-montagem/pre-montagem.service';
import { Observable, of } from 'rxjs';
import { catchError, take } from 'rxjs/operators';
import { PreMontagemParametrizacaoFormularioDialogComponent } from '../pre-montagem-parametrizacao-formulario/pre-montagem-parametrizacao-formulario.component';
import { IPreMontagemParametrizacao } from './../../../shared/interfaces/PreMontagem';
import { SnackbarService } from './../../../shared/services/snackbar/snackbar.service';

@Component({
  selector: 'app-pre-montagem-parametrizacao-list',
  templateUrl: './pre-montagem-parametrizacao-list.component.html',
  styleUrls: ['./pre-montagem-parametrizacao-list.component.scss'],
})
export class PreMontagemParametrizacaoListComponent
  implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Output() recarregarEmitter: EventEmitter<any> = new EventEmitter();
  @Output() changePageEmitter: EventEmitter<PageEvent> = new EventEmitter();
  @Input() loadPreMontagem = false;
  @Input() pageSize = 5;
  @Input() pageIndex = 0;
  @Input() loaded = false;
  preMontagemList = [];
  _premontagemResponse = null;
  totalElements: number;
  pageSizeOptions = itensPorPagina;
  displayedColumns: string[] = colunasPreMontagemParametrizacao;
  dataSource: MatTableDataSource<IPreMontagemParametrizacao>;
  selection = new SelectionModel<IPreMontagemParametrizacao>(false, []);

  get preMontagemResponse(): any {
    return this.preMontagemResponse;
  }

  @Input() set preMontagemResponse(value: any) {
    this._premontagemResponse = value;
    this.updatePaginationValues(value);
  }

  constructor(
    public dialog: MatDialog,
    private snackbar: SnackbarService,
    private preMontagemService: PreMontagemService
  ) {
    this.dataSource = new MatTableDataSource<IPreMontagemParametrizacao>(
      this.preMontagemList
    );
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  updatePaginationValues(value: any): void {
    this.totalElements = value?.totalElements || 0;
    this.preMontagemList = value?.content || [];
    this.pageIndex = 0;
    this.updateDataSource();
  }

  updateDataSource(): void {
    this.dataSource.data = this.preMontagemList;
    this.selection = new SelectionModel<IPreMontagemParametrizacao>(false, []);
  }

  handleChangePage(pageEvent: PageEvent): void {
    this.changePageEmitter.emit(pageEvent);
  }

  recarregar(): void {
    this.paginator.firstPage();
    this.recarregarEmitter.emit();
  }

  abrirFormulario(): void {
    const dialogRef = this.dialog.open(PreMontagemParametrizacaoFormularioDialogComponent, {
      width: '95vw',
      maxWidth: '95vw',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.recarregar();
      }
    });
  }

  abrirFormularioAlteracao(): void {
    const dialogRef = this.dialog.open(PreMontagemParametrizacaoFormularioDialogComponent, {
      width: '95vw',
      maxWidth: '95vw',
      data: this.selection.selected[0],
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.recarregar();
      }
    });
  }

  removerPreMontagem(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        titulo: `Deseja remover o registro: Código Sequencial: ${this.selection.selected[0].cdSeqModalidade} ?`,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.preMontagemService
          .deleteParametrizacao(this.selection.selected[0].cdSeqModalidade)
          .pipe(
            take(1),
            catchError((e) => this.handleError(e))
          )
          .subscribe(() => {
            this.recarregar();
          });
      }
    });
  }

  handleError(error: any): Observable<any> {
    this.snackbar.showSnackbarError(error.message);
    return of();
  }
}
