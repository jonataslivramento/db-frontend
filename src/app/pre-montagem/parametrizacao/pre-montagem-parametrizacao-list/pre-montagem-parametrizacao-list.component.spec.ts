import { PreMontagemParametrizacaoListComponent } from './pre-montagem-parametrizacao-list.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';



describe('PreMontagemParametrizacaoListComponent', () => {
  let component: PreMontagemParametrizacaoListComponent;
  let fixture: ComponentFixture<PreMontagemParametrizacaoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreMontagemParametrizacaoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreMontagemParametrizacaoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
