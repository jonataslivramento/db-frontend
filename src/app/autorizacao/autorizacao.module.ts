import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutorizacaoRoutingModule } from './autorizacao-routing.module';
import { AutorizacaoComponent } from './autorizacao.component';
import { SharedModule } from '@shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AutorizacaoComponent],
  imports: [
    CommonModule,
    AutorizacaoRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class AutorizacaoModule { }
