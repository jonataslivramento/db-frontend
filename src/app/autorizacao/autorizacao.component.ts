import { MatPaginator } from '@angular/material/paginator';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { concatMap, take, finalize } from 'rxjs/operators';

import { AutorizacaoService } from '@shared/services/autorizacao/autorizacao.service';
import { itensPorPagina, colunasAutorizacao } from '@shared/constants';
import { ConsultaService } from '@shared/services/consulta/consulta.service';
import { SnackbarService } from '@shared/services/snackbar/snackbar.service';
import { DialogService } from '@shared/services/dialog/dialog.service';
import { converDateTimeStringToDateTimeBRString } from '@shared/functions/date';


@Component({
  selector: 'app-autorizacao',
  templateUrl: './autorizacao.component.html',
  styleUrls: ['./autorizacao.component.scss']
})
export class AutorizacaoComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource<any>;
  form: FormGroup;
  pageNumber: number;
  pageSize: number;
  pageSizeOptions: number[];
  totalElements: number;
  lastDate: string;
  showSpinner: boolean;
  selection: SelectionModel<any>;
  displayedColumns: string[];

  constructor(
    private autorizacaoService: AutorizacaoService,
    private formBuilder: FormBuilder,
    private consultaService: ConsultaService,
    private snackbar: SnackbarService,
    private dialogService: DialogService) {
    this.dataSource = new MatTableDataSource<any>([]);
    this.pageNumber = 0;
    this.pageSize = 5;
    this.pageSizeOptions = itensPorPagina;
    this.totalElements = 0;
    this.lastDate = '';
    this.showSpinner = false;
    this.displayedColumns = colunasAutorizacao;
    this.selection = new SelectionModel<any>(false, []);
    this.form = this.formBuilder.group({
      status: '',
    });
  }

  ngOnInit(): void {
    this.getReferenceDate();
  }

  clearFields(): void {
    this.form.reset();
    this.dataSource = new MatTableDataSource<any>([]);
    this.selection = new SelectionModel<any>(false, []);
  }

  getReferenceDate(): void {
    this.consultaService.getReferenceDate()
      .pipe(take(1))
      .subscribe((response: any) => this.lastDate = response.date, err => this.snackbar.showSnackbarError(err.error.message));
  }

  getOperations(): void {
    this.showSpinner = true;
    this.pageNumber = 0;
    this.paginator.firstPage();
    this.autorizacaoService.getOperations(this.form.value.status, this.lastDate, this.pageSize, this.pageNumber)
      .pipe(
          take(1),
          finalize(() => this.showSpinner = false)
        )
      .subscribe((response) => {
        this.dataSource = new MatTableDataSource(response.content);
        this.dataSource.sort = this.sort;
        this.totalElements = response.totalElements;
        this.showSpinner = false;
      }, err => this.snackbar.showSnackbarError(err.error.message))
  }

  converDateTimeStringToDateTimeBRString(value: string): string {
    return converDateTimeStringToDateTimeBRString(value);
  }

  rejectOperation(): void {
    const autCdSequencial = this.selection.selected[0].autCdSequencial;

    this.dialogService.openConfirmationDialog({
      titulo: 'Rejeitar Operação:',
      acao: 'Confirmar',
      dados: [{ item: 'Código da operação', autCdSequencial }],
      format: {
        key: 'item',
        value: 'autCdSequencial',
      },
    })
      .pipe(
        concatMap(confirm => confirm && this.autorizacaoService.rejectOperation(autCdSequencial).pipe(take(1)))
      )
      .subscribe(() => {
        this.dialogService.openSuccessDialog({
          titulo: 'Operação rejeitada com sucesso:',
          dados: [{ item: 'Código da operação', autCdSequencial }],
          format: {
            key: 'item',
            value: 'autCdSequencial',
          },
        });

        this.getOperations();
      }, err => this.snackbar.showSnackbarError(err.error.message));
  }

  aproveOperation(): void {
    const autCdSequencial = this.selection.selected[0].autCdSequencial;

    this.dialogService.openConfirmationDialog({
      titulo: 'Aprovar Operação:',
      acao: 'Confirmar',
      dados: [{ item: 'Código da operação', autCdSequencial }],
      format: {
        key: 'item',
        value: 'autCdSequencial',
      },
    })
      .pipe(
        concatMap(confirm => confirm && this.autorizacaoService.aproveOperation(autCdSequencial).pipe(take(1)))
      )
      .subscribe(() => {
        this.dialogService.openSuccessDialog({
          titulo: 'Operação aprovada com sucesso:',
          dados: [{ item: 'Código da operação', autCdSequencial }],
          format: {
            key: 'item',
            value: 'autCdSequencial',
          },
        });

        this.getOperations();
      }, err => this.snackbar.showSnackbarError(err.error.message));
  }

  changePage(event: any): void {
    this.pageNumber = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getOperations();
  }

}
