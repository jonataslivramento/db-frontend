import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AutorizacaoComponent } from './autorizacao.component';

const routes: Routes = [{ path: '', component: AutorizacaoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutorizacaoRoutingModule { }
