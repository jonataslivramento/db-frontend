import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { ExtractType } from '@shared/enums/ExtractType';
import { ConsolidateAndAnlytical } from '@shared/interfaces/ConsolidatedAndAnalyticalExtract';
import { ExtratoService } from '@shared/services/extrato/extrato.service';
import { itensPorPagina, colunasConsultaConsolidadaEAnalitica } from '@shared/constants';
import { take } from 'rxjs/operators';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { UtilsService } from '@shared/services/utils/utils.service';
import { SnackbarService } from '@shared/services/snackbar/snackbar.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { ConsultaService } from '@shared/services/consulta/consulta.service';
import { Company } from '@shared/interfaces/Company';
import { Modality } from '@shared/interfaces/Modality';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.scss']
})
export class ConsultaComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageIndex: number;
  pageSize: number;
  pageSizeOptions: number[];
  totalElements: number;
  searchForm: FormGroup;
  extractPdfForm: FormGroup;
  extractType: ExtractType;
  lastDate: string;
  showSearchSpinner: boolean;
  showExtractSpinner: boolean;
  displayedColumns: string[];
  dataSource: MatTableDataSource<ConsolidateAndAnlytical>;
  companies: Company[];
  modalities$: Observable<Modality[]>;

  constructor(
    private formBuilder: FormBuilder,
    private extratoService: ExtratoService,
    private util: UtilsService,
    private snackbar: SnackbarService,
    private consultaService: ConsultaService
  ) {
    this.dataSource = new MatTableDataSource<ConsolidateAndAnlytical>([]);
    this.pageSize = 5;
    this.pageIndex = 0;
    this.pageSizeOptions = itensPorPagina;
    this.totalElements = 0;
    this.extractType = ExtractType.ANALYTIC;
    this.lastDate = '';
    this.displayedColumns = colunasConsultaConsolidadaEAnalitica;
    this.showSearchSpinner = false;
    this.showExtractSpinner = false;
    this.searchForm = this.formBuilder.group({
      codigoEmpresa: null,
      codigoCliente: null,
      modalidade: 'all',
      tipoDeRelatorio: 'MONTHLY_REPORT',
    });

    this.extractPdfForm = this.formBuilder.group({
      ordenadoPor: 'cdCliente'
    });
  }

  ngOnInit(): void {
    this.getCompanies();
    this.getModalities();
    this.getReferenceDate();
  }

  initialStateSearchForm(): void {
    this.searchForm.reset();
    this.searchForm.get('codigoEmpresa')?.setValue(101101);
    this.searchForm.get('tipoDeRelatorio')?.setValue('MONTHLY_REPORT');
    this.searchForm.get('modalidade')?.setValue('all');
    this.dataSource = new MatTableDataSource();
    this.pageSize = 5;
    this.pageIndex = 0;
    this.totalElements = 0;
  }

  extractToggle(event: MatButtonToggleChange): void {
    this.extractType = event.value;
    this.initialStateSearchForm();
  }

  clearFields(): void {
    this.initialStateSearchForm();
  }

  handleChangePage(pageEvent: PageEvent): void {
    this.pageIndex = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;
    this.getExtractClient();
  }

  getCompanies(): void {
    this.consultaService.getCompanies()
      .pipe(take(1))
      .subscribe(response => {
        this.companies = response;
        this.searchForm.get('codigoEmpresa')?.setValue(101101);
      });
  }

  getModalities(): void {
    this.modalities$ = this.consultaService.getModalities();
  }

  getReferenceDate(): void {
    this.consultaService.getReferenceDate()
      .pipe(take(1))
      .subscribe(response => {
        this.lastDate = response.date;
      });
  }

  onSearchSubmit(): void {
    this.pageIndex = 0;
    this.paginator.firstPage();
    this.getExtractClient();
  }

  getExtractClient(): void {
    this.showSearchSpinner = true;

    this.extratoService.getConsolidadeAndAnlyticalExtract(
      this.searchForm.get('codigoEmpresa')?.value,
      this.searchForm.get('codigoCliente')?.value,
      this.searchForm.get('modalidade')?.value,
      this.searchForm.get('tipoDeRelatorio')?.value,
      this.extractType,
      this.pageIndex,
      this.pageSize,
      this.lastDate
    )
      .pipe(take(1))
      .subscribe((response) => {
        this.dataSource = new MatTableDataSource(response.content);
        this.dataSource.sort = this.sort;
        this.totalElements = response.totalElements;
        this.showSearchSpinner = false;
      },
        error => {
          this.snackbar.showSnackbarInfo(error.error.message);
          this.dataSource = new MatTableDataSource();
          this.showSearchSpinner = false;
        });
  }

  onExtractPdfSubmit(): void {
    this.showExtractSpinner = true;

    this.extratoService.getConsolidadeAndAnlyticalExtractPdf(
      this.searchForm.get('codigoEmpresa')?.value,
      this.searchForm.get('codigoCliente')?.value,
      this.searchForm.get('modalidade')?.value,
      this.searchForm.get('tipoDeRelatorio')?.value,
      this.extractType,
      this.extractPdfForm.get('ordenadoPor')?.value,
      this.lastDate
    )
      .pipe(take(1))
      .subscribe(response => {
        this.showExtractSpinner = false;
        this.util.onDownloadFile(response, 'application/pdf', 'EXT_DBSA.pdf');
      },
        error => {
          console.error(error);
          this.snackbar.showSnackbarError(error.error.message);
          this.showExtractSpinner = false;
        });
  }

  get isExtractTypeAnalitic(): boolean {
    return this.extractType === ExtractType.ANALYTIC;
  }
}
