import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '@shared/shared.module';
import { ConsultaComponent } from './consulta.component';

describe('ConsultaComponent', () => {
  let component: ConsultaComponent;
  let fixture: ComponentFixture<ConsultaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, SharedModule, ReactiveFormsModule, BrowserAnimationsModule],
      declarations: [ConsultaComponent],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should execute the function clearFields', () => {
    component.ngOnInit();

    const value = component.searchForm.get('codigoEmpresa')?.value;

    component.clearFields();

    expect(value).toEqual(null);
  });

  it('should execute the function onSearchSubmit', () => {
    component.ngOnInit();

    component.onSearchSubmit();

    expect(component.dataSource.data.length).toBeGreaterThanOrEqual(0);
  });

  it('should execute the function onExtractPdfSubmit', () => {
    component.ngOnInit();

    component.onExtractPdfSubmit();

    expect(component.showExtractSpinner).toBeTrue();
  });

  it('should execute the function handleChangePage', () => {
    component.handleChangePage({ pageIndex: 0, pageSize: 5, previousPageIndex: 5, length: 5 });

    expect(component.pageSize).toBe(5);
  });

  it('should execute the function extractToggle', () => {
    const element: HTMLElement | null = document.querySelector('#consolidate-button');

    console.log(element);
    element?.click();

    expect(component.extractType).toBe('CONSOLIDATE');
  });
});
