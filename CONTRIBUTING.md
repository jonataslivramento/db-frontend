# Contribution guide

## CI/CD

This repository it's configured to use Gitlab pipelines as a way to build, test and publish its assets.

See `.gitlab-ci.yml`

### CI/CD Environments

The following environments variable need to be configured in the project settings:

- `NEXUS_TOKEN` - Authentication token with access to deploy on Nexus, execute the following command to generate the token:

> echo -n 'admin:admin123' | openssl base64

See: https://help.sonatype.com/repomanager3/formats/npm-registry#npmRegistry-AuthenticationUsingBasicAuth

### REFERENCES

- https://guides.sonatype.com/repo3/quick-start-guides/proxying-maven-and-npm/

